<?php

// https://www.strangebuzz.com/fr/snippets/ma-configuration-easydeploy
use EasyCorp\Bundle\EasyDeployBundle\Configuration\DefaultConfiguration;
use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class() extends DefaultDeployer {
    public function configure(): DefaultConfiguration
    {
        $configuration = $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('jack@51.210.109.242:1435')
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/var/www/html/quicksilver')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@github.com:jackP8000/tikare.git')
            // the repository branch to deploy
            ->repositoryBranch('master')
            ->symfonyEnvironment('prod')
        ;

        return $this->forceEnvironmentEnvVarName($configuration);
    }

    private function forceEnvironmentEnvVarName(DefaultConfiguration $configuration): DefaultConfiguration
    {
        $property = (new ReflectionClass(get_class($configuration)))->getProperty('_symfonyEnvironmentEnvVarName');
        $property->setAccessible(true);
        $property->setValue($configuration, 'FAKE_APP_ENV');
        $property->setAccessible(false);

        return $configuration;
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
        $this->runRemote('cp /var/www/html/quicksilver/.env.local /var/www/html/quicksilver/repo/.env.local');
    }
};
