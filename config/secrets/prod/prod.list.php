<?php

return [
    'AIRTABLE_APIKEY' => null,
    'APP_SECRET' => null,
    'DATABASE_URL' => null,
    'DB_NAME' => null,
    'DB_PASSWORD' => null,
    'DB_USER' => null,
    'MAILER_DSN' => null,
    'SENDGRID_APIKEY' => null,
    'SENDGRID_NEWSLETTER_LIST' => null,
    'SENDGRID_REGISTERED_USERS_LIST' => null,
    'SENTRY_DSN' => null,
    'SITE_BASE_URL' => null,
    'SLACK_DSN' => null,
];
