<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220605140026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, partner_id INT DEFAULT NULL, thematic_id INT DEFAULT NULL, air_table_id VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, picture_filename VARCHAR(255) DEFAULT NULL, contact_first_name VARCHAR(255) DEFAULT NULL, contact_last_name VARCHAR(255) DEFAULT NULL, contact_role VARCHAR(255) DEFAULT NULL, contact_phone_number VARCHAR(255) DEFAULT NULL, contact_email VARCHAR(255) DEFAULT NULL, INDEX IDX_47CC8C929393F8FE (partner_id), INDEX IDX_47CC8C922395FCED (thematic_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, street_number VARCHAR(255) DEFAULT NULL, route VARCHAR(255) DEFAULT NULL, locality VARCHAR(255) NOT NULL, postal_code VARCHAR(255) NOT NULL, lat DOUBLE PRECISION DEFAULT NULL, lng DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE change_email_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, email VARCHAR(255) NOT NULL, requested_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon_immutable)\', expires_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon_immutable)\', UNIQUE INDEX UNIQ_9AB020E9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE occurrence (id INT AUTO_INCREMENT NOT NULL, action_id INT NOT NULL, address_id INT DEFAULT NULL, total_needed_volunteers INT NOT NULL, reserved_registrations INT NOT NULL, children_minimum_age INT NOT NULL, children_maximum_number INT NOT NULL, start_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon_immutable)\', ends_at DATETIME(6) NOT NULL COMMENT \'(DC2Type:carbon_immutable)\', register_closed_at DATETIME(6) DEFAULT NULL COMMENT \'(DC2Type:carbon_immutable)\', published_at DATETIME(6) DEFAULT NULL COMMENT \'(DC2Type:carbon_immutable)\', publishable TINYINT(1) NOT NULL, cancelled TINYINT(1) NOT NULL, INDEX IDX_BEFD81F39D32F035 (action_id), INDEX IDX_BEFD81F3F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE occurrence_user (occurrence_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4D9D3E3430572FAC (occurrence_id), INDEX IDX_4D9D3E34A76ED395 (user_id), PRIMARY KEY(occurrence_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parental_consent_file (id INT AUTO_INCREMENT NOT NULL, filename VARCHAR(255) NOT NULL, original_filename VARCHAR(255) NOT NULL, mime_type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partner (id INT AUTO_INCREMENT NOT NULL, air_table_id VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, logo_filename VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registration (id INT AUTO_INCREMENT NOT NULL, occurrence_id INT NOT NULL, volunteer_id INT NOT NULL, children_number INT DEFAULT NULL, wait_listed TINYINT(1) NOT NULL, cancelled TINYINT(1) NOT NULL, cancelled_at VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_62A8A7A730572FAC (occurrence_id), INDEX IDX_62A8A7A78EFAB6B1 (volunteer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thematic (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, logo_name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, parental_consent_file_id INT DEFAULT NULL, slug VARCHAR(100) NOT NULL, email VARCHAR(180) NOT NULL, first_name VARCHAR(180) DEFAULT NULL, last_name VARCHAR(180) DEFAULT NULL, phone_number VARCHAR(180) DEFAULT NULL, birthday DATETIME(1) DEFAULT NULL COMMENT \'(DC2Type:carbon_immutable)\', district VARCHAR(180) DEFAULT NULL, profile_picture_filename VARCHAR(255) DEFAULT NULL, agreed_terms_at DATETIME(3) DEFAULT NULL COMMENT \'(DC2Type:carbon_immutable)\', image_right_clearance TINYINT(1) NOT NULL, newsletter TINYINT(1) NOT NULL, email_notification TINYINT(1) NOT NULL, sms_notification TINYINT(1) NOT NULL, parental_consent_valid TINYINT(1) NOT NULL, enabled TINYINT(1) NOT NULL, deactivated TINYINT(1) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) DEFAULT NULL, changed_password_at DATETIME(1) DEFAULT NULL COMMENT \'(DC2Type:carbon_immutable)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649989D9B62 (slug), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649538520CE (parental_consent_file_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C929393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id)');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C922395FCED FOREIGN KEY (thematic_id) REFERENCES thematic (id)');
        $this->addSql('ALTER TABLE change_email_request ADD CONSTRAINT FK_9AB020E9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE occurrence ADD CONSTRAINT FK_BEFD81F39D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE occurrence ADD CONSTRAINT FK_BEFD81F3F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE occurrence_user ADD CONSTRAINT FK_4D9D3E3430572FAC FOREIGN KEY (occurrence_id) REFERENCES occurrence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE occurrence_user ADD CONSTRAINT FK_4D9D3E34A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A730572FAC FOREIGN KEY (occurrence_id) REFERENCES occurrence (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A78EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649538520CE FOREIGN KEY (parental_consent_file_id) REFERENCES parental_consent_file (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE occurrence DROP FOREIGN KEY FK_BEFD81F39D32F035');
        $this->addSql('ALTER TABLE occurrence DROP FOREIGN KEY FK_BEFD81F3F5B7AF75');
        $this->addSql('ALTER TABLE occurrence_user DROP FOREIGN KEY FK_4D9D3E3430572FAC');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A730572FAC');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649538520CE');
        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C929393F8FE');
        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C922395FCED');
        $this->addSql('ALTER TABLE change_email_request DROP FOREIGN KEY FK_9AB020E9A76ED395');
        $this->addSql('ALTER TABLE occurrence_user DROP FOREIGN KEY FK_4D9D3E34A76ED395');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A78EFAB6B1');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE change_email_request');
        $this->addSql('DROP TABLE occurrence');
        $this->addSql('DROP TABLE occurrence_user');
        $this->addSql('DROP TABLE parental_consent_file');
        $this->addSql('DROP TABLE partner');
        $this->addSql('DROP TABLE registration');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE thematic');
        $this->addSql('DROP TABLE user');
    }
}
