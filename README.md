# [TiKare](https://github.com/fabianrupin195/tikare)

[Tikare](https://github.com/fabianrupin195/tikare) is a web application developed for the association [TiKare](https://tikare.fr).
allowing the  for the management of voluntary solidarity action.

## Components and Versions
This project was created on April 11, 2020, with the following components: 
* Symfony 5.0.9, installed with website-skeleton dependencies
* other Symfony components : 
  * Symfony Mailer
  

## Getting Started to contribute

To begin using this project, 
* Clone the repo: `git clone git@gitlab.com:fabianrupin/tikare.git`
* install back dependencies : `composer install`
* install front dependencies and build front files : 
  * `yarn install`
  * `yarn build`
* Copy `.env` to `.env.local` file and change `APP_ENV` variable value to `dev
* Copy `.env.dev` to `.env.dev.local` file and change variables values to suit your your configuration :
  * BDD credentials
  * Sendgrid API Key
  * Slack WebHook
* create and migrate the bdd : 
  * `./bin/console doctrine:database:create`
  * `./bin/console doctrine:migrations:migrate`
  * `./bin/console doctrine:fixtures:load`
* clear the cache : `./bin/console c:c`

## Putting the app into production
* install back dependencies : `composer install --no-dev --optimize-autoloader`
* install front dependencies and build front files :
  * `yarn install --force`
  * `yarn build`
* Deploy Secrets to production
  * Update or adding secrets production values with `secrets set`
  * Rotate production decryption key with `secrets:generate-keys --rotate`
  * Copy the production decryption key `config/secrets/prod/prod.decrypt.private.php` to your server.
* create and migrate the bdd :
  * `./bin/console doctrine:database:create`
  * `./bin/console doctrine:migrations:migrate`
* Configure apache configuration
  * In `public` folder copy `.htaccess.dist` to `.htaccess`
  * Comment `Require valid-user` line to remove access with password
* clear the cache : `./bin/console c:c`

## Bugs and Issues

Have a bug or an issue with this project? 
[Open a new issue](https://github.com/fabianrupin195/tikare/issues/new) here on github.

## Creator

Tikare was created by and is maintained by 
**[Fabian Rupin](https://wheelwork.io/)**, Owner of [WheelWork](https://wheelwork.io/).

## Copyright and License
