<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomepageControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testHomepagePublicShow()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'LE BÉNÉVOLAT POUR TOUS.TES À RENNES');
        $this->assertSelectorTextContains('#calendar', "Voir encore plus d'action");
        $this->assertSelectorNotExists('i.icofont-logout');
        $this->assertSelectorTextContains('#calendar > div > div.text-center > a',
            "Voir encore plus d'action"
        );
    }

    public function testHomepageLoggedInShow()
    {
        $client = $this->logIn('jane.doe@mail.com', 'mypass');

        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('i.icofont-logout');
    }

    public function testCalendarPreview()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertSelectorTextContains('h4.card-title > a',
            'Réhabilitation de logement'
        );
        $this->assertSelectorTextContains('#calendar > div > div:nth-child(1) > div.col-md-4:nth-child(1)',
            'Logement'
        );
        $this->assertSelectorTextContains('#calendar > div > div:nth-child(1) > div.col-md-4:nth-child(1)',
            'Inscriptions clôturées'
        );
        $this->assertSelectorTextContains('#calendar > div > div:nth-child(1) > div.col-md-4:nth-child(3)',
            'Complet'
        );
    }

    public function testNewsletterRegistration()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $buttonCrawlerNode = $crawler->filter('button')->last();
        $form = $buttonCrawlerNode->form([
            'newsletter_subscription_form[email]' => 'inconnu@mail.com',
        ]);
        $client->submit($form);

        $client->followRedirect();

        $this->assertSelectorTextContains('body > div.alert-success',
            'Votre inscription à la newsletter a bien été prise en compte !'
        );
    }
}
