<?php

namespace App\Tests\Controller\Calendar;

use App\Tests\Controller\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OccurrenceControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testCalendarShow()
    {
        $client = static::createClient();

        $client->request('GET', '/calendrier');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'actions à venir');
        $this->assertSelectorTextContains('h4.card-title > a', 'Réhabilitation de logement');
        $this->assertSelectorTextContains('#navbarResponsive a', 'Voir les actions');
        $this->assertSelectorTextContains('#navbarResponsive [type=button]', 'Se connecter');

        $client->request('GET', '/calendrier?search=logement');
        $this->assertSelectorTextContains('h1', '3 actions à venir');

        /*        $client->request('GET', '/calendrier?search=eolienne');
                $this->assertSelectorTextContains('h1', '0 actions à venir');*/
    }

    public function testOccurrenceShow()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/calendrier/occurrence/1');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'RÉHABILITATION DE LOGEMENT');

        // ToDo: test pour vérifier qu'on affiche pas les occurences pas encore en ligne
    }
}
