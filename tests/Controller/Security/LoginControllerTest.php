<?php

namespace App\Tests\Controller\Security;

use App\Tests\Controller\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{
    use AuthenticationTrait;

    public function testShowLoginPage()
    {
        $client = static::createClient();

        $client->request('GET', '/se-connecter');

        $this->assertResponseIsSuccessful();
    }

    public function testAdminLogin()
    {
        $client = $this->logIn('admin@mail.com', 'mypass');

        $client->followRedirect();

        $client->request('GET', '/admin/utilisateurs/liste');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testReferrerLogin()
    {
        $client = $this->logIn('referrer@mail.com', 'mypass');

        $client->followRedirect();

        $client->request('GET', '/inscription/liste');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUserLogin()
    {
        $client = $this->logIn('jane.doe@mail.com', 'mypass');

        $client->followRedirect();

        $client->request('GET', '/inscription/liste');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
