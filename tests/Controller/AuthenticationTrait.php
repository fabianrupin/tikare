<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait AuthenticationTrait
{
    private function logIn(string $email, string $password): KernelBrowser
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/se-connecter');

        $buttonCrawlerNode = $crawler->selectButton('Valider');

        $form = $buttonCrawlerNode->form([
            'user_login[email]' => $email,
            'user_login[plainPassword]' => $password,
        ]);

        $client->submit($form);

        return $client;
    }
}
