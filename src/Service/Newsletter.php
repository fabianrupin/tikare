<?php

namespace App\Service;

class Newsletter
{
    public function __construct(private readonly SendGrid $sendGrid, private readonly string $sendGridNewsletterListID)
    {
    }

    public function subscribe(string $email): void
    {
        $this->sendGrid->createNewContact($email, $this->sendGridNewsletterListID);
    }

    public function unsubscribe(string $email): void
    {
        $this->sendGrid->removeContactFromList($email, $this->sendGridNewsletterListID);
    }
}
