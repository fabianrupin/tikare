<?php

namespace App\Service\Mailer;

use App\Entity\Occurrence;
use App\Entity\Registration;
use App\Entity\User;
use App\Model\DTO\Calendar\Occurrence\ContactFormDTO;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActionMailer
{
    public function __construct(private readonly MailerInterface $mailer, private readonly TranslatorInterface $translator, private readonly string $tikareEmail, private readonly string $tikareName)
    {
    }

    public function sendOnNewActionRegistrationToVolunteer(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($registration->getVolunteer()->getEmail()))
            ->subject($this->translator->trans('action.new_registration.to_volunteer.subject', [], 'email'))
            ->htmlTemplate('email/occurrence/to_volunteer/new_registration.html.twig')
            ->context([
                'emailTo' => $registration->getVolunteer()->getEmail(),
                'registration' => $registration, ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendOnNewActionRegistrationToReferrer(Registration $registration, User $referrer): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($referrer->getEmail()))
            ->subject($this->translator->trans('action.new_registration.to_referrer.subject', [], 'email'))
            ->htmlTemplate('email/occurrence/to_referrer/new_registration.html.twig')
            ->context([
                'emailTo' => $registration->getVolunteer()->getEmail(),
                'registration' => $registration, ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendOnOccurrenceRegistrationEditionToVolunteer(Registration $registration): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($registration->getVolunteer()->getEmail()))
            ->subject($this->translator->trans('action.edit_registration.to_volunteer.subject', [], 'email'))
            ->htmlTemplate('email/occurrence/to_volunteer/edit_registration.html.twig')
            ->context([
                'emailTo' => $registration->getVolunteer()->getEmail(),
                'registration' => $registration, ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendOnOccurrenceRegistrationEditionToReferrer(Registration $registration, User $referrer): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($referrer->getEmail()))
            ->subject($this->translator->trans('action.edit_registration.to_referrer.subject', [], 'email'))
            ->htmlTemplate('email/occurrence/to_referrer/edit_registration.html.twig')
            ->context([
                'emailTo' => $registration->getVolunteer()->getEmail(),
                'registration' => $registration, ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendOnOccurrenceCancellation(Occurrence $occurrence): void
    {
        foreach ($occurrence->getRegistrations() as $registration) {
            $email = (new TemplatedEmail())
                ->to(new Address($registration->getVolunteer()->getEmail()))
                ->subject($this->translator->trans('action.occurrence_cancellation.subject', [], 'email'))
                ->htmlTemplate('email/occurrence/to_volunteer/cancel_occurrence.html.twig')
                ->context([
                    'emailTo' => $registration->getVolunteer()->getEmail(),
                    'registration' => $registration, ]);
            $this->mailer->send($email);
        }
    }

    public function sendOnParentalConsentUploadToAdmin(User $admin, User $user): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($admin->getEmail()))
            ->subject($this->translator->trans('user.parental_consent_upload.to_admin.subject', [], 'email'))
            ->htmlTemplate('email/user/to_admin/parental_consent_upload.html.twig')
            ->context([
                'emailTo' => $admin->getEmail(),
                'user' => $user, ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendMessageToReferrer(ContactFormDTO $contactFormDTO, Registration $registration): void
    {
        foreach ($registration->getOccurrence()->getReferrers() as $referrer) {
            $email = (new TemplatedEmail())
                ->to(new Address($referrer->getEmail(), $referrer->getFirstName()))
                ->subject($this->translator->trans('action.message_to_referrer.subject', [], 'email'))
                ->htmlTemplate('email/occurrence/to_referrer/contact.html.twig')
                ->context([
                    'contactFormDTO' => $contactFormDTO,
                    'emailTo' => $referrer->getEmail(),
                    'registration' => $registration,
                ]);
            $this->mailer->send($email);
        }
    }
}
