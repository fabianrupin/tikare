<?php

namespace App\Service\Mailer;

use App\Entity\ChangeEmailRequest;
use App\Entity\User;
use App\Model\DTO\Homepage\contactFormDTO;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\VerifyEmail\Model\VerifyEmailSignatureComponents;

class UserMailer
{
    public function __construct(private readonly MailerInterface $mailer, private readonly TranslatorInterface $translator, private readonly string $tikareEmail, private readonly string $tikareName)
    {
    }

    public function sendContactMessage(contactFormDTO $contactFormDTO): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($this->tikareEmail, $this->tikareName))
            ->subject($this->translator->trans('mailer.contact-message.subject', [], 'service'))
            ->htmlTemplate('email/contact.html.twig')
            ->context(['contactFormDTO' => $contactFormDTO, 'emailTo' => $this->tikareEmail]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendConfirmationNewsletterSubscription(string $email): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($email))
            ->subject($this->translator->trans('mailer.newsletter-subscription.subject', [], 'service'))
            ->htmlTemplate('email/newsletter_subscription.html.twig')
            ->context(['emailTo' => $email]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendUserRegistrationConfirmation(User $user): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($user->getEmail()))
            ->subject($this->translator->trans('mailer.registration-confirmation.subject', [], 'service'))
            ->htmlTemplate('email/user/registration_confirmation.html.twig')
            ->context(['emailTo' => $user->getEmail(), 'user' => $user]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendPasswordResetRequest(User $user, ResetPasswordToken $resetToken, int $tokenLifetime)
    {
        $email = (new TemplatedEmail())
            ->to(new Address($user->getEmail(), $user->getFirstName()))
            ->subject($this->translator->trans('mailer.password-reset-request.subject', [], 'service'))
            ->htmlTemplate('email/user/reset_password.html.twig')
            ->context([
                'emailTo' => $user->getEmail(),
                'resetToken' => $resetToken,
                'tokenLifetime' => $tokenLifetime,
            ]);
        $this->mailer->send($email);

        return $email;
    }

    public function sendEmailConfirmation(ChangeEmailRequest $changeEmailRequest, VerifyEmailSignatureComponents $signatureComponents): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->to(new Address($changeEmailRequest->getEmail(), $changeEmailRequest->getUser()->getFirstName()))
            ->subject('Please Confirm your Email')
            ->htmlTemplate('email/user/confirmation_email.html.twig')
            ->context(['emailTo' => $changeEmailRequest->getEmail()]);

        $context = $email->getContext();
        $context['signedUrl'] = $signatureComponents->getSignedUrl();
        $context['expiresAt'] = $signatureComponents->getExpiresAt();

        $email->context($context);

        $this->mailer->send($email);

        return $email;
    }
}
