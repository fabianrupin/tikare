<?php

namespace App\Service;

use App\Model\DTO\Homepage\NewsletterSubscriptionFormDTO;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AirTable
{
    private HttpClientInterface $client;

    public function __construct(
        private readonly string $airtableApiKey,
        private readonly string $airtableOrganization,
        private readonly string $airtableAction,
        private readonly string $airtablePeople,
        private readonly string $airtableNewsletter)
    {
        $this->client = HttpClient::create();
    }

    public function addNewsletterSubscriber(NewsletterSubscriptionFormDTO $subscriptionFormDTO): int
    {
        $response = $this->client->request('POST', $this->airtableNewsletter, [
            'auth_bearer' => $this->airtableApiKey,
            'json' => [
                'records' => [
                    ['fields' => ['Email' => $subscriptionFormDTO->getEmail()],
                    ],
                ],
            ],
        ]);

        return $response->getStatusCode();
    }

    public function getPartnerInformation(string $partnerId)
    {
        return $this->client->request('GET', $this->airtableOrganization.$partnerId, [
            'auth_bearer' => $this->airtableApiKey,
        ]);
    }

    public function getPartnersList()
    {
        return $this->client->request('GET', $this->airtableOrganization.'?view=Liste des partenaires site Internet', [
            'auth_bearer' => $this->airtableApiKey,
        ]);
    }

    public function getActionsList()
    {
        return $this->client->request('GET', $this->airtableAction, [
            'auth_bearer' => $this->airtableApiKey,
        ]);
    }

    public function getActionInformation(string $actionId)
    {
        return $this->client->request('GET', $this->airtableAction.$actionId, [
            'auth_bearer' => $this->airtableApiKey,
        ]);
    }

    public function getPeopleInformation(string $peopleId)
    {
        return $this->client->request('GET', $this->airtablePeople.$peopleId, [
            'auth_bearer' => $this->airtableApiKey,
        ]);
    }
}
