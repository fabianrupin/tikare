<?php

namespace App\Service;

use App\Entity\Registration;
use App\Entity\User;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Chatter
{
    public function __construct(private readonly TranslatorInterface $translator, private readonly NotifierInterface $notifier)
    {
    }

    public function sendNewsletterSubscriptionHook(string $email): void
    {
        $this->send($this->translator->trans(
            'slack.newsletter-subscription',
            ['%mail%' => $email],
            'service'));
    }

    public function sendUserRegistrationConfirmation(User $user): void
    {
        $this->send($this->translator->trans(
            'slack.user-registration-confirmation',
            [
                '%firstname%' => $user->getFirstName(),
                '%lastname%' => $user->getLastName(),
                '%email%' => $user->getEmail(),
            ],
            'service'));
    }

    public function sendActionRegistrationHook(Registration $registration): void
    {
        $this->send($this->translator->trans(
            'slack.action-registration-confirmation',
            [
                '%firstname%' => $registration->getVolunteer()->getFirstName(),
                '%lastname%' => $registration->getVolunteer()->getLastName(),
                '%phone_number%' => $registration->getVolunteer()->getPhoneNumber(),
                '%occurrence%' => $registration->getOccurrence(),
            ],
            'service'));
    }

    public function sendActionCancellationHook(Registration $registration): void
    {
        $this->send($this->translator->trans(
            'slack.action-registration-cancellation',
            [
                '%firstname%' => $registration->getVolunteer()->getFirstName(),
                '%lastname%' => $registration->getVolunteer()->getLastName(),
                '%phone_number%' => $registration->getVolunteer()->getPhoneNumber(),
                '%occurrence%' => $registration->getOccurrence(),
            ],
            'service'));
    }

    private function send(string $message): void
    {
        $notification = new Notification($message, ['chat/slack']);
        $this->notifier->send($notification);
    }
}
