<?php

namespace App\Service;

use Gedmo\Sluggable\Util\Urlizer;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    final public const PROFILE_PICTURE = 'profile_picture';

    final public const ACTION_PICTURE = 'action_picture';

    final public const PARTNER_LOGO = 'partner_logo';

    final public const PARENTAL_CONSENT = 'parental_consent';

    private FilesystemInterface $privateFilesystem;

    public function __construct(FilesystemInterface $publicUploadFilesystem, FilesystemInterface $privateUploadFilesystem, private RequestStackContext $requestStackContext, private LoggerInterface $logger, private string $uploadedAssetsBaseUrl)
    {
        $this->filesystem = $publicUploadFilesystem;
        $this->privateFilesystem = $privateUploadFilesystem;
    }

    public function uploadLogoPartner(File $file, ?string $existingFilename): string
    {
        $destination = self::PARTNER_LOGO;

        return $this->uploadPublicFile($file, $destination, $existingFilename);
    }

    public function uploadPictureProfile(File $file, ?string $existingFilename): string
    {
        $destination = self::PROFILE_PICTURE;

        return $this->uploadPublicFile($file, $destination, $existingFilename);
    }

    public function uploadPictureAction(File $file, ?string $existingFilename): string
    {
        $destination = self::ACTION_PICTURE;

        return $this->uploadPublicFile($file, $destination, $existingFilename);
    }

    public function uploadParentalConsent(File $file): string
    {
        $destination = self::PARENTAL_CONSENT;

        return $this->uploadPrivateFile($file, $destination);
    }

    private function uploadPublicFile(File $file, string $destination, ?string $existingFilename): string
    {
        $newFilename = $this->uploadFile($file, $destination, true);

        if ($existingFilename) {
            try {
                $result = $this->filesystem->delete($destination.'/'.$existingFilename);

                if (false === $result) {
                    throw new \Exception(sprintf('Could not delete old uploaded file "%s"', $existingFilename));
                }
            } catch (FileNotFoundException) {
                $this->logger->alert(sprintf('Old uploaded file "%s" missing when trying to delete', $existingFilename));
            }
        }

        return $newFilename;
    }

    private function uploadPrivateFile(File $file, string $destination): string
    {
        return $this->uploadFile($file, $destination, false);
    }

    public function getPublicPath(string $path): string
    {
        return $this->requestStackContext->getBasePath().$this->publicAssetsBaseUrl.'/'.$path;
    }

    private function uploadFile(File $file, string $directory, bool $isPublic): string
    {
        if ($file instanceof UploadedFile) {
            $originalFilename = $file->getClientOriginalName();
        } else {
            $originalFilename = $file->getFilename();
        }

        $filesystem = $isPublic ? $this->filesystem : $this->privateFilesystem;

        $newFilename = Urlizer::urlize(pathinfo($originalFilename, PATHINFO_FILENAME)).'-'.uniqid().'.'.$file->guessExtension();

        $stream = fopen($file->getPathname(), 'r');

        $result = $filesystem->writeStream(
            $directory.'/'.$newFilename,
            $stream
        );

        if (false === $result) {
            throw new \Exception(sprintf('Could not write uploaded file"%s"', $newFilename));
        }

        if (is_resource($stream)) {
            fclose($stream);
        }

        return $newFilename;
    }

    /**
     * @return resource
     */
    public function readStream(string $path, bool $isPublic)
    {
        $filesystem = $isPublic ? $this->filesystem : $this->privateFilesystem;

        $resource = $filesystem->readStream($path);

        if (false === $resource) {
            throw new \Exception(sprintf('Error opening stream for "%s"', $path));
        }

        return $resource;
    }
}
