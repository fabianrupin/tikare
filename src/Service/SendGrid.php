<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SendGrid
{
    public function __construct(private readonly string $sendGridAPIKey, private readonly string $sendGridContactEndPoint, private readonly string $sendGridListEndPoint, private readonly string $sendGridRegisteredUserListID, private readonly LoggerInterface $httpClientLogger)
    {
    }

    public function subscribeRegisteredUser(string $email): int
    {
        return $this->createNewContact(
            $email,
            $this->sendGridRegisteredUserListID
        );
    }

    public function createNewContact(string $email, string $list): int
    {
        $client = HttpClient::create();
        try {
            $response = $client->request('PUT', $this->sendGridContactEndPoint, [
                'auth_bearer' => $this->sendGridAPIKey,
                'json' => [
                    'list_ids' => [$list],
                    'contacts' => [
                        ['email' => $email],
                    ],
                ],
            ]);

            return $response->getStatusCode();
        } catch (TransportExceptionInterface $e) {
            $this->httpClientLogger->error('Failed create SendGrid contact ('.$email.'): '.$e->getMessage());

            return $e->getCode();
        }
    }

    public function removeContactFromList(string $email, string $list): int
    {
        $client = HttpClient::create();
        try {
            $contact_id = $this->searchContact($email);

            try {
                $response = $client->request('DELETE', $this->sendGridListEndPoint.'/'.$list.'/contacts', [
                    'auth_bearer' => $this->sendGridAPIKey,
                    'query' => [
                        'contact_ids' => $contact_id,
                    ],
                ]);

                return $response->getStatusCode();
            } catch (TransportExceptionInterface $e) {
                $this->httpClientLogger->error('Failed remove SendGrid contact : '.$e->getMessage());

                return $e->getCode();
            }
        } catch (\Exception $e) {
            $this->httpClientLogger->error($e->getMessage());

            return $e->getCode();
        }
    }

    public function searchContact(string $email): string
    {
        $client = HttpClient::create();

        try {
            $response = $client->request('POST', $this->sendGridContactEndPoint.'/search', [
                'auth_bearer' => $this->sendGridAPIKey,
                'json' => [
                    'query' => "email LIKE '$email%'",
                ],
            ]);
            $result = json_decode($response->getContent(), null, 512, JSON_THROW_ON_ERROR);
        } catch (HttpExceptionInterface|TransportExceptionInterface $e) {
            $this->httpClientLogger->error('Failed to search SendGrid contact ('.$email.'): '.$e->getMessage());

            return $e->getCode();
        }

        if (1 === !$result->contact_count) {
            throw new \Exception('User not found', 404);
        }

        return $result->result[0]->id;
    }
}
