<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateAdminCommand extends Command
{
    protected static $defaultName = 'app:create-admin';

    public function __construct(private readonly UserRepository $userRepository, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Create the first admin user')
            ->setHelp('This command allows you to create the first account, at the very beginning. 
            It doesn\'t work if an another admin account already exist')
            ->addArgument('email', InputArgument::REQUIRED, 'First Admin email')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $users = $this->userRepository->findAll();
        foreach ($users as $user) {
            if (in_array('ROLE_ADMIN', $user->getRoles())) {
                $io->error('An Admin user already exist. Reset database if you want use this command');

                return 0;
            }
        }

        foreach ($users as $user) {
            if ($user->getEmail() === $input->getArgument('email')) {
                $user->upToAdmin();

                $this->userRepository->save($user);

                $io->success('You have successfully upgrade this user '.$user->getEmail().' as admin. Password has not been updated');

                return 0;
            }
        }

        if (!$input->getArgument('password')) {
            $io->error('If the user does not exist, you must provide a password as second argument.');
        }
        $user = User::createFirstAdmin($input->getArgument('email'), $input->getArgument('password'));

        $this->userRepository->save($user);

        $io->success('You have successfully added the new admin user: '.$user->getEmail());

        return 0;
    }
}
