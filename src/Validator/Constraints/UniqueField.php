<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class UniqueField extends Constraint
{
    public $message = 'notUniqueField';

    public $entityClass = null;

    public $fieldName = null;

    public $fieldGetter = null;

    public $existingEntityIdGetter = null;

    public $entityPrimaryKeyFieldName = 'id';

    public function getRequiredOptions(): array
    {
        return ['entityClass', 'fieldName', 'fieldGetter'];
    }

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
