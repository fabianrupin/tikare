<?php

declare(strict_types=1);

namespace App\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueFieldValidator extends ConstraintValidator
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $valueObject The value that should be validated
     */
    public function validate($valueObject, Constraint $constraint)
    {
        $valueGetter = $constraint->fieldGetter;
        $value = $valueObject->$valueGetter();

        if (null === $value || '' === $value) {
            return;
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->entityManager->getRepository($constraint->entityClass);
        $nbOthersEntitiesWithTheSameFieldValueQueryBuilder = $entityRepository
            ->createQueryBuilder('entity')
            ->select('COUNT(entity)')
            ->andWhere(sprintf('entity.%s = :fieldValue', $constraint->fieldName))->setParameter('fieldValue', $value)
        ;

        if (null !== $constraint->existingEntityIdGetter) {
            $existingEntityIdGetter = $constraint->existingEntityIdGetter;
            $existingEntityId = $valueObject->$existingEntityIdGetter();
            $nbOthersEntitiesWithTheSameFieldValueQueryBuilder
                ->andWhere(sprintf('entity.%s != :existingEntityId', $constraint->entityPrimaryKeyFieldName))
                ->setParameter('existingEntityId', $existingEntityId)
            ;
        }

        $nbOthersEntitiesWithTheSameFieldValue = (int) $nbOthersEntitiesWithTheSameFieldValueQueryBuilder
            ->getQuery()->getSingleScalarResult()
        ;

        if ($nbOthersEntitiesWithTheSameFieldValue > 0) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%fieldName%', $constraint->fieldName)
                ->atPath($constraint->fieldName)
                ->setInvalidValue($value)
                ->addViolation()
            ;
        }
    }
}
