<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class FrenchMobilePhoneNumber extends Constraint
{
    public string $message = 'register.phone.invalid';
}
