<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CarbonDate extends Constraint
{
    public string $message = 'register.date.invalid';
}
