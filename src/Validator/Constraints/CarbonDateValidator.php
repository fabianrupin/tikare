<?php

namespace App\Validator\Constraints;

use Carbon\CarbonImmutable;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CarbonDateValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value) {
            if (!$value instanceof CarbonImmutable) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}
