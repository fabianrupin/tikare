<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class FrenchMobilePhoneNumberValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value) {
            if (!preg_match("#^0[6-7]\d{8}$#", (string) $value)) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}
