<?php

namespace App\Entity;

use App\Model\DTO\Admin\User\AbstractUserDTO;
use App\Model\DTO\Admin\User\AdminCreateUserDTO;
use App\Model\DTO\Admin\User\AdminEditUserDTO;
use App\Model\DTO\User\ChangePasswordDTO;
use App\Model\DTO\User\EditUserDTO;
use App\Model\DTO\User\UserRegistrationDTO;
use App\Service\UploaderHelper;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: 'App\Repository\UserRepository')]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Stringable
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[Gedmo\Slug(fields: ['firstName', 'lastName'])]
    #[ORM\Column(length: 100, unique: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $firstName = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $lastName = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $phoneNumber = null;

    #[ORM\Column(type: 'carbon_immutable', length: 180, precision: 1, nullable: true)]
    private ?CarbonImmutable $birthday = null;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $district = null;

    #[ORM\Column(nullable: true)]
    private ?string $profilePictureFilename = null;

    #[ORM\Column(type: 'carbon_immutable', length: 180, precision: 3, nullable: true)]
    private ?CarbonImmutable $agreedTermsAt = null;

    #[ORM\Column()]
    private bool $imageRightClearance = true;

    #[ORM\Column()]
    private bool $newsletter = true;

    #[ORM\Column()]
    private bool $emailNotification = true;

    #[ORM\Column()]
    private bool $smsNotification = true;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?ParentalConsentFile $parentalConsentFile = null;

    #[ORM\Column()]
    private bool $parentalConsentValid = true;

    #[ORM\Column()]
    private bool $enabled = true;

    #[ORM\Column()]
    private bool $deactivated = false;

    #[ORM\Column(type: 'json')]
    private ?array $roles = [];

    #[ORM\Column(nullable: true)]
    private ?string $password = null;

    #[ORM\Column(type: 'carbon_immutable', length: 180, precision: 1, nullable: true)]
    private ?CarbonImmutable $changedPasswordAt = null;

    private ?string $plainPassword = null;

    #[ORM\ManyToMany(targetEntity: Occurrence::class, mappedBy: 'referrers')]
    private Collection $occurrences;

    #[ORM\OneToMany(mappedBy: 'volunteer', targetEntity: Registration::class)]
    private Collection $registrations;

    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    private ?ChangeEmailRequest $changeEmailRequest;

    public function __construct()
    {
        $this->occurrences = new ArrayCollection();
        $this->registrations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->firstName.' '.$this->lastName.' ('.$this->birthday->format('d M Y').')';
    }

    public static function create(UserRegistrationDTO $userDTO): User
    {
        $user = new self();
        $user->email = $userDTO->getEmail();
        $user->plainPassword = $userDTO->getPlainPassword();
        $user->firstName = $userDTO->getFirstName();
        $user->lastName = $userDTO->getLastName();
        $user->phoneNumber = $userDTO->getPhoneNumber();
        $user->birthday = $userDTO->getBirthday();
        $user->district = $userDTO->getDistrict();
        $user->changedPasswordAt = CarbonImmutable::now();
        if ($userDTO->isAgreedTerms()) {
            $user->agreedTermsAt = CarbonImmutable::now();
        }
        if ($userDTO->getBirthday() > new CarbonImmutable('- 18 years')) {
            $user->parentalConsentValid = false;
        }

        return $user;
    }

    public static function createFromAdmin(AdminCreateUserDTO $userDTO): User
    {
        $user = new self();
        $user->email = $userDTO->getEmail();
        $user->plainPassword = 'Ti'.random_int(5, 25).'Kare?';
        $user->firstName = $userDTO->getFirstName();
        $user->lastName = $userDTO->getLastName();
        $user->phoneNumber = $userDTO->getPhoneNumber();
        $user->birthday = $userDTO->getBirthday();
        $user->district = $userDTO->getDistrict();
        $user->roles = $userDTO->getRoles();

        if ($userDTO->getBirthday() > new CarbonImmutable('- 18 years')) {
            $user->parentalConsentValid = false;
        }

        return $user;
    }

    public function edit(AbstractUserDTO $userDTO)
    {
        $this->firstName = $userDTO->getFirstName();
        $this->lastName = $userDTO->getLastName();
        $this->phoneNumber = $userDTO->getPhoneNumber();
        $this->birthday = $userDTO->getBirthday();
        $this->district = $userDTO->getDistrict();

        if ($userDTO instanceof EditUserDTO) {
            $this->imageRightClearance = $userDTO->isImageRightClearance();
            $this->newsletter = $userDTO->isNewsletter();
        }
        if ($userDTO instanceof AdminEditUserDTO) {
            $this->email = $userDTO->getEmail();
            $this->roles = $userDTO->getRoles();
        }
    }

    public static function createFirstAdmin(string $email, string $password): User
    {
        $user = new self();
        $user->email = $email;
        $user->plainPassword = $password;
        $user->roles = ['ROLE_ADMIN'];

        return $user;
    }

    public function changePassword(ChangePasswordDTO $changePasswordDTO): self
    {
        $this->plainPassword = $changePasswordDTO->getPlainPassword();
        $this->changedPasswordAt = CarbonImmutable::now();

        return $this;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function deactivate(): void
    {
        $this->deactivated = true;
    }

    public function reactivate(): void
    {
        $this->deactivated = false;
    }

    public function parentalConsentValidate(): void
    {
        $this->parentalConsentValid = true;
    }

    public function setProfilePictureFilename(?string $profilePictureFilename): void
    {
        $this->profilePictureFilename = $profilePictureFilename;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getBirthday(): ?CarbonImmutable
    {
        return $this->birthday;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function getProfilePictureFilename(): ?string
    {
        return $this->profilePictureFilename;
    }

    public function getProfilePicturePath()
    {
        return UploaderHelper::PROFILE_PICTURE.'/'.$this->getProfilePictureFilename();
    }

    public function isImageRightClearance(): bool
    {
        return $this->imageRightClearance;
    }

    public function isNewsletter(): bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): void
    {
        $this->newsletter = $newsletter;
    }

    public function isEmailNotification(): bool
    {
        return $this->emailNotification;
    }

    public function isSmsNotification(): bool
    {
        return $this->smsNotification;
    }

    public function getParentalConsentFile(): ?ParentalConsentFile
    {
        return $this->parentalConsentFile;
    }

    public function isParentalConsentValid(): bool
    {
        return $this->parentalConsentValid;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function isDeactivated(): bool
    {
        return $this->deactivated;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function getChangedPasswordAt(): ?CarbonImmutable
    {
        return $this->changedPasswordAt;
    }

    public function getAgreedTermsAt(): ?CarbonImmutable
    {
        return $this->agreedTermsAt;
    }

    /**
     * @return Collection|Occurrence[]
     */
    public function getOccurrences(): Collection
    {
        return $this->occurrences;
    }

    public function addOccurrence(Occurrence $occurrence): self
    {
        if (!$this->occurrences->contains($occurrence)) {
            $this->occurrences[] = $occurrence;
            $occurrence->addReferrer($this);
        }

        return $this;
    }

    public function removeOccurrence(Occurrence $occurrence): self
    {
        if ($this->occurrences->contains($occurrence)) {
            $this->occurrences->removeElement($occurrence);
            $occurrence->removeReferrer($this);
        }

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function getChangeEmailRequest(): ?ChangeEmailRequest
    {
        return $this->changeEmailRequest;
    }

    public function setChangeEmailRequest(ChangeEmailRequest $changeEmailRequest): self
    {
        $this->changeEmailRequest = $changeEmailRequest;

        // set the owning side of the relation if necessary
        if ($changeEmailRequest->getUser() !== $this) {
            $changeEmailRequest->setUser($this);
        }

        return $this;
    }

    public function setParentalConsentFile(?ParentalConsentFile $parentalConsentFile): void
    {
        $this->parentalConsentFile = $parentalConsentFile;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function setBirthday(?CarbonImmutable $birthday): void
    {
        $this->birthday = $birthday;
    }

    public function setDistrict(?string $district): void
    {
        $this->district = $district;
    }

    public function setAgreedTermsAt(?CarbonImmutable $agreedTermsAt): void
    {
        $this->agreedTermsAt = $agreedTermsAt;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function setImageRightClearance(bool $imageRightClearance): void
    {
        $this->imageRightClearance = $imageRightClearance;
    }
}
