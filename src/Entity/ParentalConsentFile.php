<?php

namespace App\Entity;

use App\Model\DTO\User\UploadParentalConsentDTO;
use App\Repository\ParentalConsentFileRepository;
use App\Service\UploaderHelper;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParentalConsentFileRepository::class)]
class ParentalConsentFile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column()]
    private ?string $filename = null;

    #[ORM\Column()]
    private ?string $originalFilename = null;

    #[ORM\Column()]
    private ?string $mimeType = null;

    public static function create(UploadParentalConsentDTO $parentalConsentDTO, string $filename): self
    {
        $parentalConsent = new self();
        $parentalConsent->filename = $filename;
        $parentalConsent->originalFilename = $parentalConsentDTO->getParentalConsentFile()->getClientOriginalName();
        $parentalConsent->mimeType = $parentalConsentDTO->getParentalConsentFile()->getMimeType();

        return $parentalConsent;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getFilePath(): string
    {
        return UploaderHelper::PARENTAL_CONSENT.'/'.$this->getFilename();
    }
}
