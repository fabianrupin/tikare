<?php

namespace App\Entity;

use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;
use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?string $streetNumber = null;

    #[ORM\Column(nullable: true)]
    private ?string $route = null;

    #[ORM\Column()]
    private ?string $locality = null;

    #[ORM\Column()]
    private ?string $postalCode = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $lat = null;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $lng = null;

    public function __toString(): string
    {
        return $this->getStreetNumber().' '.$this->getRoute().' '.$this->getPostalCode().' '.$this->getLocality();
    }

    public static function create(AdminOccurrenceDTO $occurrenceDTO): self
    {
        $address = new self();

        $address->streetNumber = $occurrenceDTO->getStreetNumber();
        $address->route = $occurrenceDTO->getRoute();
        $address->locality = $occurrenceDTO->getLocality();
        $address->postalCode = $occurrenceDTO->getPostalCode();

        return $address;
    }

    public function edit(AdminOccurrenceDTO $occurrenceDTO)
    {
        $this->streetNumber = $occurrenceDTO->getStreetNumber();
        $this->route = $occurrenceDTO->getRoute();
        $this->locality = $occurrenceDTO->getLocality();
        $this->postalCode = $occurrenceDTO->getPostalCode();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(string $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }
}
