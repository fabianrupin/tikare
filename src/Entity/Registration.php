<?php

namespace App\Entity;

use App\Model\DTO\Admin\Registration\AdminRegistrationDTO;
use App\Model\DTO\Calendar\Registration\RegistrationDTO;
use App\Repository\RegistrationRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

#[ORM\Entity(repositoryClass: RegistrationRepository::class)]
class Registration
{
    use TimestampableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'registrations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Occurrence $occurrence = null;

    #[ORM\ManyToOne(inversedBy: 'registrations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $volunteer = null;

    #[ORM\Column(nullable: true)]
    private ?int $childrenNumber = null;

    #[ORM\Column()]
    private bool $waitListed = false;

    #[ORM\Column()]
    private bool $cancelled = false;

    #[ORM\Column(nullable: true)]
    private ?CarbonImmutable $cancelledAt = null;

    public function getStatus(): string
    {
        if ($this->cancelled) {
            return 'cancelled';
        }
        if ($this->waitListed) {
            return 'waitListed';
        }
        if ($this->occurrence->getEndsAt() < Carbon::now()) {
            return 'completed';
        }

        return 'confirmed';
    }

    public static function create(RegistrationDTO $registrationDTO, Occurrence $occurrence, User $volunteer): Registration
    {
        $registration = new self();

        $registration->volunteer = $volunteer;
        $registration->occurrence = $occurrence;
        $registration->childrenNumber = $registrationDTO->getChildrenNumber();

        return $registration;
    }

    public function edit(RegistrationDTO $registrationDTO): void
    {
        $this->childrenNumber = $registrationDTO->getChildrenNumber();
    }

    public static function createFromAdmin(AdminRegistrationDTO $registrationDTO): Registration
    {
        $registration = new self();

        $registration->volunteer = $registrationDTO->getVolunteer();
        $registration->occurrence = $registrationDTO->getOccurrence();

        $registration->childrenNumber = $registrationDTO->getChildrenNumber();

        return $registration;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOccurrence(): ?Occurrence
    {
        return $this->occurrence;
    }

    public function setOccurrence(?Occurrence $occurrence): self
    {
        $this->occurrence = $occurrence;

        return $this;
    }

    public function getVolunteer(): ?User
    {
        return $this->volunteer;
    }

    public function getChildrenNumber(): int
    {
        if (!$this->childrenNumber) {
            return 0;
        }

        return $this->childrenNumber;
    }

    public function isWaitListed(): bool
    {
        return $this->waitListed;
    }

    public function isCancelled(): ?bool
    {
        return $this->cancelled;
    }

    public function cancel(): void
    {
        if ($this->occurrence->getEndsAt() < Carbon::now()) {
            return;
        }
        $this->cancelled = true;
        $this->cancelledAt = CarbonImmutable::now();
    }

    public function unCancel(): void
    {
        $this->cancelled = false;
    }

    public function getCancelledAt(): ?CarbonImmutable
    {
        return $this->cancelledAt;
    }

    public function setCancelledAt(?CarbonImmutable $cancelledAt): self
    {
        $this->cancelledAt = $cancelledAt;

        return $this;
    }
}
