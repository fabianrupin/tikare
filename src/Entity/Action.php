<?php

namespace App\Entity;

use App\Model\DTO\Admin\Action\AdminActionDTO;
use App\Repository\ActionRepository;
use App\Service\UploaderHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ActionRepository::class)]
class Action implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?string $airTableId = null;

    #[ORM\Column()]
    private ?string $title = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?string $pictureFilename = null;

    #[ORM\Column(nullable: true)]
    private ?string $contactFirstName = null;

    #[ORM\Column(nullable: true)]
    private ?string $contactLastName = null;

    #[ORM\Column(nullable: true)]
    private ?string $contactRole = null;

    #[ORM\Column(nullable: true)]
    private ?string $contactPhoneNumber = null;

    #[ORM\Column(nullable: true)]
    private ?string $contactEmail = null;

    #[ORM\ManyToOne(inversedBy: 'actions')]
    private ?Partner $partner = null;

    #[ORM\OneToMany(mappedBy: 'action', targetEntity: Occurrence::class, orphanRemoval: true)]
    private Collection $occurrences;

    #[ORM\ManyToOne(inversedBy: 'action')]
    private ?Thematic $thematic = null;

    public function __construct()
    {
        $this->occurrences = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->title;
    }

    public static function create(AdminActionDTO $actionDTO): Action
    {
        $action = new self();

        $action->title = $actionDTO->getTitle();
        $action->description = $actionDTO->getDescription();
        $action->contactFirstName = $actionDTO->getContactFirstName();
        $action->contactLastName = $actionDTO->getContactLastName();
        $action->contactRole = $actionDTO->getContactRole();
        $action->contactPhoneNumber = $actionDTO->getContactPhoneNumber();
        $action->contactEmail = $actionDTO->getContactEmail();
        $action->partner = $actionDTO->getPartner();
        $action->thematic = $actionDTO->getThematic();
        $action->airTableId = $actionDTO->getAirtableId();

        return $action;
    }

    public function edit(AdminActionDTO $actionDTO): void
    {
        $this->title = $actionDTO->getTitle();
        $this->description = $actionDTO->getDescription();
        $this->contactFirstName = $actionDTO->getContactFirstName();
        $this->contactLastName = $actionDTO->getContactLastName();
        $this->contactRole = $actionDTO->getContactRole();
        $this->contactPhoneNumber = $actionDTO->getContactPhoneNumber();
        $this->contactEmail = $actionDTO->getContactEmail();
        $this->partner = $actionDTO->getPartner();
        $this->thematic = $actionDTO->getThematic();
    }

    public function setPictureFilename(?string $pictureFilename): void
    {
        $this->pictureFilename = $pictureFilename;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAirTableId(): ?string
    {
        return $this->airTableId;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPictureFilename(): ?string
    {
        return $this->pictureFilename;
    }

    public function getPicturePath()
    {
        return UploaderHelper::ACTION_PICTURE.'/'.$this->getPictureFilename();
    }

    public function getContactFirstName(): ?string
    {
        return $this->contactFirstName;
    }

    public function getContactLastName(): ?string
    {
        return $this->contactLastName;
    }

    public function getContactRole(): ?string
    {
        return $this->contactRole;
    }

    public function getContactPhoneNumber(): ?string
    {
        return $this->contactPhoneNumber;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * @return Collection|Occurrence[]
     */
    public function getOccurrences(): Collection
    {
        return $this->occurrences;
    }

    public function addOccurrence(Occurrence $occurrence): self
    {
        if (!$this->occurrences->contains($occurrence)) {
            $this->occurrences[] = $occurrence;
            $occurrence->setAction($this);
        }

        return $this;
    }

    public function removeOccurrence(Occurrence $occurrence): self
    {
        if ($this->occurrences->contains($occurrence)) {
            $this->occurrences->removeElement($occurrence);
            // set the owning side to null (unless already changed)
            if ($occurrence->getAction() === $this) {
                $occurrence->setAction(null);
            }
        }

        return $this;
    }

    public function getThematic(): ?Thematic
    {
        return $this->thematic;
    }

    public function setThematic(?Thematic $thematic): self
    {
        $this->thematic = $thematic;

        return $this;
    }
}
