<?php

namespace App\Entity;

use App\Model\DTO\User\ChangeEmailDTO;
use App\Repository\ChangeEmailRequestRepository;
use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChangeEmailRequestRepository::class)]
class ChangeEmailRequest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'changeEmailRequest')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column()]
    private ?string $email = null;

    #[ORM\Column(type: 'carbon_immutable')]
    private ?CarbonImmutable $requestedAt = null;

    #[ORM\Column(type: 'carbon_immutable')]
    private ?CarbonImmutable $expiresAt = null;

    public static function create(ChangeEmailDTO $changeEmailDTO, User $user): self
    {
        $changeEmailRequest = new self();
        $changeEmailRequest->user = $user;
        $changeEmailRequest->email = $changeEmailDTO->getEmail();
        $changeEmailRequest->requestedAt = CarbonImmutable::now();
        $changeEmailRequest->expiresAt = $changeEmailRequest->requestedAt->addHours(2);

        return $changeEmailRequest;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRequestedAt(): ?\DateTimeInterface
    {
        return $this->requestedAt;
    }

    public function setRequestedAt(\DateTimeInterface $requestedAt): self
    {
        $this->requestedAt = $requestedAt;

        return $this;
    }

    public function isExpired(): bool
    {
        if ($this->expiresAt > CarbonImmutable::now()) {
            return false;
        }

        return false;
    }

    public function setExpiresAt($expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }
}
