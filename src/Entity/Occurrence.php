<?php

namespace App\Entity;

use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;
use App\Repository\OccurrenceRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OccurrenceRepository::class)]
class Occurrence implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column()]
    private ?int $totalNeededVolunteers = null;

    #[ORM\Column()]
    private ?int $reservedRegistrations = null;

    #[ORM\Column()]
    private ?int $childrenMinimumAge = null;

    #[ORM\Column()]
    private ?int $childrenMaximumNumber = null;

    #[ORM\Column(type: 'carbon_immutable')]
    private ?CarbonImmutable $startAt = null;

    #[ORM\Column(type: 'carbon_immutable')]
    private ?CarbonImmutable $endsAt = null;

    #[ORM\Column(type: 'carbon_immutable', nullable: true)]
    private ?CarbonImmutable $registerClosedAt = null;

    #[ORM\Column(type: 'carbon_immutable', nullable: true)]
    private ?CarbonImmutable $publishedAt = null;

    #[ORM\Column()]
    private bool $publishable = false;

    #[ORM\Column()]
    private bool $cancelled = false;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'occurrences')]
    private $referrers;

    #[ORM\ManyToOne(inversedBy: 'occurrences')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Action $action = null;

    #[ORM\OneToMany(mappedBy: 'occurrence', targetEntity: Registration::class, orphanRemoval: true)]
    private Collection $registrations;

    #[ORM\ManyToOne()]
    #[ORM\JoinColumn(nullable: true)]
    private ?Address $address = null;

    public function __construct()
    {
        $this->referrers = new ArrayCollection();
        $this->registrations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->startAt->format('d-M H:i').' - '.$this->action->getTitle();
    }

    public function isOpen(): bool
    {
        if (
            $this->isPublishable()
            && ($this->registerClosedAt > Carbon::now())
            && ($this->getRemainingNeededVolunteers() > 0)
            && (!$this->isCancelled())
        ) {
            return true;
        }

        return false;
    }

    public static function create(AdminOccurrenceDTO $occurrenceDTO, Address $address): Occurrence
    {
        $occurrence = new self();

        $occurrence->totalNeededVolunteers = $occurrenceDTO->getTotalNeededVolunteers();
        $occurrence->reservedRegistrations = $occurrenceDTO->getReservedRegistrations();
        $occurrence->childrenMinimumAge = $occurrenceDTO->getChildrenMinimumAge();
        $occurrence->childrenMaximumNumber = $occurrenceDTO->getChildrenMaximumNumber();
        $occurrence->startAt = $occurrenceDTO->getStartAt();
        $occurrence->endsAt = $occurrenceDTO->getEndsAt();
        $occurrence->registerClosedAt = $occurrenceDTO->getRegisterClosedAt();
        $occurrence->publishedAt = $occurrenceDTO->getPublishedAt();
        $occurrence->publishable = $occurrenceDTO->isPublishable();
        $occurrence->referrers = $occurrenceDTO->getReferrers();
        $occurrence->action = $occurrenceDTO->getAction();
        $occurrence->address = $address;

        return $occurrence;
    }

    public function edit(AdminOccurrenceDTO $occurrenceDTO): void
    {
        $this->totalNeededVolunteers = $occurrenceDTO->getTotalNeededVolunteers();
        $this->reservedRegistrations = $occurrenceDTO->getReservedRegistrations();
        $this->childrenMinimumAge = $occurrenceDTO->getChildrenMinimumAge();
        $this->childrenMaximumNumber = $occurrenceDTO->getChildrenMaximumNumber();
        $this->startAt = $occurrenceDTO->getStartAt();
        $this->endsAt = $occurrenceDTO->getEndsAt();
        $this->registerClosedAt = $occurrenceDTO->getRegisterClosedAt();
        $this->publishedAt = $occurrenceDTO->getPublishedAt();
        $this->publishable = $occurrenceDTO->isPublishable();
        $this->referrers = $occurrenceDTO->getReferrers();
        $this->action = $occurrenceDTO->getAction();
    }

    public function cancel(): void
    {
        $this->cancelled = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalNeededVolunteers(): ?int
    {
        return $this->totalNeededVolunteers;
    }

    public function getReservedRegistrations(): ?int
    {
        return $this->reservedRegistrations;
    }

    public function getRemainingNeededVolunteers(): ?int
    {
        $realPlacesWithoutRegistrations = $this->totalNeededVolunteers - $this->reservedRegistrations;

        $registrationWithChildren = 0;
        foreach ($this->registrations as $registration) {
            if (!$registration->isCancelled()) {
                $registrationWithChildren += 1 + $registration->getChildrenNumber();
            }
        }

        return $realPlacesWithoutRegistrations - $registrationWithChildren;
    }

    public function getChildrenMinimumAge(): ?int
    {
        return $this->childrenMinimumAge;
    }

    public function getChildrenMaximumNumber(): ?int
    {
        return $this->childrenMaximumNumber;
    }

    public function getRemainingChildrenPlaces(): ?int
    {
        $childPlaces = 0;
        foreach ($this->registrations as $registration) {
            if (!$registration->isCancelled()) {
                $childPlaces = +$registration->getChildrenNumber();
            }
        }

        return $this->getChildrenMaximumNumber() - $childPlaces;
    }

    public function getStartAt(): ?CarbonImmutable
    {
        return $this->startAt;
    }

    public function getEndsAt(): ?CarbonImmutable
    {
        return $this->endsAt;
    }

    public function getDuration(): string
    {
        return $this->endsAt->locale('fr_FR')->diffForHumans($this->startAt, true, true, 2);
    }

    public function getRegisterClosedAt(): ?CarbonImmutable
    {
        return $this->registerClosedAt;
    }

    public function getPublishedAt(): ?CarbonImmutable
    {
        return $this->publishedAt;
    }

    public function isPublishable(): bool
    {
        if ($this->referrers->isEmpty()) {
            return false;
        }

        return $this->publishable;
    }

    public function isCancelled(): bool
    {
        return $this->cancelled;
    }

    /**
     * @return Collection|User[]
     */
    public function getReferrers(): Collection
    {
        return $this->referrers;
    }

    public function addReferrer(User $referrer): self
    {
        if (!$this->referrers->contains($referrer)) {
            $this->referrers[] = $referrer;
        }

        return $this;
    }

    public function removeReferrer(User $referrer): self
    {
        if ($this->referrers->contains($referrer)) {
            $this->referrers->removeElement($referrer);
        }

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations[] = $registration;
            $registration->setOccurrence($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->contains($registration)) {
            $this->registrations->removeElement($registration);
            // set the owning side to null (unless already changed)
            if ($registration->getOccurrence() === $this) {
                $registration->setOccurrence(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }
}
