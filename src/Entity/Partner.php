<?php

namespace App\Entity;

use App\Model\DTO\Admin\Partner\AdminCreatePartnerDTO;
use App\Model\DTO\Admin\Partner\AdminEditPartnerDTO;
use App\Repository\PartnerRepository;
use App\Service\UploaderHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PartnerRepository::class)]
class Partner implements \Stringable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?string $airTableId = null;

    #[ORM\Column()]
    private ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?string $website = null;

    #[ORM\Column(nullable: true)]
    private ?string $logoFilename = null;

    #[ORM\OneToMany(mappedBy: 'partner', targetEntity: Action::class)]
    private Collection $actions;

    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public static function create(AdminCreatePartnerDTO $partnerDTO): self
    {
        $partner = new self();

        $partner->airTableId = $partnerDTO->getAirTableId();
        $partner->name = $partnerDTO->getName();
        $partner->description = $partnerDTO->getDescription();
        $partner->website = $partnerDTO->getWebsite();

        return $partner;
    }

    public function edit(AdminEditPartnerDTO $partnerDTO): void
    {
        $this->airTableId = $partnerDTO->getAirTableId();
        $this->name = $partnerDTO->getName();
        $this->description = $partnerDTO->getDescription();
        $this->website = $partnerDTO->getWebsite();
    }

    public function setLogoFilename(?string $logoFilename): void
    {
        $this->logoFilename = $logoFilename;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAirTableId(): ?string
    {
        return $this->airTableId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getLogoFilename(): ?string
    {
        return $this->logoFilename;
    }

    public function getLogoPath()
    {
        return UploaderHelper::PARTNER_LOGO.'/'.$this->getLogoFilename();
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setPartner($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->contains($action)) {
            $this->actions->removeElement($action);
            // set the owning side to null (unless already changed)
            if ($action->getPartner() === $this) {
                $action->setPartner(null);
            }
        }

        return $this;
    }
}
