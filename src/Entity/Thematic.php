<?php

namespace App\Entity;

use App\Repository\ThematicRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ThematicRepository::class)]
class Thematic
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column()]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'thematic', targetEntity: Action::class)]
    private Collection $action;

    #[ORM\Column()]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?string $logoName = null;

    public function __construct()
    {
        $this->action = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Action[]
     */
    public function getAction(): Collection
    {
        return $this->action;
    }

    public function addAction(Action $action): self
    {
        if (!$this->action->contains($action)) {
            $this->action[] = $action;
            $action->setThematic($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->action->contains($action)) {
            $this->action->removeElement($action);
            // set the owning side to null (unless already changed)
            if ($action->getThematic() === $this) {
                $action->setThematic(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoName(?string $logoName): self
    {
        $this->logoName = $logoName;

        return $this;
    }
}
