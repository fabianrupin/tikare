<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class NewsletterRegistrationEvent extends Event
{
    public function __construct(private readonly string $email)
    {
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
