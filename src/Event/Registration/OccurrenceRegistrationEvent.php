<?php

namespace App\Event\Registration;

use App\Entity\Registration;
use Symfony\Contracts\EventDispatcher\Event;

class OccurrenceRegistrationEvent extends Event
{
    public function __construct(private readonly Registration $registration)
    {
    }

    public function getRegistration(): Registration
    {
        return $this->registration;
    }
}
