<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\Action;
use App\Model\DTO\Admin\Action\AdminActionDTO;

class Actions
{
    public static function create(array $arguments): Action
    {
        $actionDTO = new AdminActionDTO();

        $actionDTO->setAirTableId($arguments['airtable_id']);
        $actionDTO->setTitle($arguments['title']);
        $actionDTO->setDescription($arguments['description']);
        $actionDTO->setContactFirstName($arguments['firstname']);
        $actionDTO->setContactLastName($arguments['lastname']);
        $actionDTO->setContactRole($arguments['role']);
        $actionDTO->setContactPhoneNumber($arguments['phone']);
        $actionDTO->setContactEmail($arguments['email']);
        $actionDTO->setPartner($arguments['partner']);
        $actionDTO->setThematic($arguments['thematic']);

        return Action::create($actionDTO);
    }
}
