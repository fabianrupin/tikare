<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\Partner;
use App\Model\DTO\Admin\Partner\AdminCreatePartnerDTO;

class Partners
{
    public static function create(array $arguments): Partner
    {
        $partnerDTO = new AdminCreatePartnerDTO();

        $partnerDTO->setAirTableId($arguments['airtable_id']);
        $partnerDTO->setName($arguments['name']);
        $partnerDTO->setDescription($arguments['description']);
        $partnerDTO->setWebsite($arguments['website']);

        return Partner::create($partnerDTO);
    }
}
