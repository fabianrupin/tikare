<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\Occurrence;
use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;

class Occurrences
{
    public static function create(array $arguments): Occurrence
    {
        $occurrenceDTO = new AdminOccurrenceDTO();

        $occurrenceDTO->setTotalNeededVolunteers($arguments['total_needed_volunteers']);
        $occurrenceDTO->setReservedRegistrations($arguments['reserved_registrations']);
        $occurrenceDTO->setChildrenMinimumAge($arguments['children_minimum_age']);
        $occurrenceDTO->setChildrenMaximumNumber($arguments['children_maximum_number']);
        $occurrenceDTO->setStartAt($arguments['start_at']);

        $endsAt = $occurrenceDTO->getStartAt()->addHours($arguments['duration_in_hours']);
        $occurrenceDTO->setEndsAt($endsAt->toDate());

        $closedAt = $occurrenceDTO->getStartAt()->subHours($arguments['closed_before_in_hours']);
        $occurrenceDTO->setRegisterClosedAt($closedAt->toDate());

        $occurrenceDTO->setPublishedAt($arguments['publishable_at']);
        $occurrenceDTO->setPublishable($arguments['publishable']);
        $occurrenceDTO->setReferrers($arguments['referrers']);

        $actionKey = array_rand($arguments['action'], 1);
        $occurrenceDTO->setAction($arguments['action'][$actionKey]);

        return Occurrence::create($occurrenceDTO, $arguments['address']);
    }
}
