<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\User;
use App\Model\DTO\User\EditUserDTO;
use App\Model\DTO\User\UserRegistrationDTO;

class Users
{
    public static function create(array $arguments): User
    {
        $userRegistrationDTO = new UserRegistrationDTO();

        $userRegistrationDTO->setEmail($arguments['email']);
        $userRegistrationDTO->setFirstname($arguments['firstname']);
        $userRegistrationDTO->setLastname($arguments['lastname']);
        $userRegistrationDTO->setBirthday($arguments['birthday']);
        $userRegistrationDTO->setPhoneNumber($arguments['phone_number']);
        $userRegistrationDTO->setPlainPassword($arguments['plain_password']);
        $userRegistrationDTO->setDistrict($arguments['district']);
        $userRegistrationDTO->setAgreedTerms(true);

        $user = User::create($userRegistrationDTO);

        $user->setRoles([$arguments['role']]);

        $editUserDTO = new EditUserDTO($user);

        $editUserDTO->setImageRightClearance($arguments['image_right_clearance']);
        $editUserDTO->setNewsletter($arguments['newsletter']);

        $user->edit($editUserDTO);

        return $user;
    }
}
