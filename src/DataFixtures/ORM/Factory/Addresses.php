<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\Address;
use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;

class Addresses
{
    public static function create(array $arguments): Address
    {
        $occurrenceDTO = new AdminOccurrenceDTO();

        $occurrenceDTO->setStreetNumber($arguments['street_number']);
        $occurrenceDTO->setRoute($arguments['route']);
        $occurrenceDTO->setLocality($arguments['locality']);
        $occurrenceDTO->setPostalCode($arguments['postal_code']);

        return Address::create($occurrenceDTO);
    }
}
