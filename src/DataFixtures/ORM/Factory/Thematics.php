<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM\Factory;

use App\Entity\Thematic;

class Thematics
{
    public static function create(array $arguments): Thematic
    {
        $thematic = new Thematic();

        $thematic->setName($arguments['name']);
        $thematic->setLogoName($arguments['logo_name']);

        return $thematic;
    }
}
