<?php

namespace App\DataFixtures;

use App\Entity\Action;
use App\Service\UploaderHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Nelmio\Alice\Loader\NativeLoader;
use Symfony\Component\HttpFoundation\File\File;

class AppFixtures extends Fixture
{
    private static array $actionPictures = [
        'bac-jardin.jpg',
        'jardin.jpeg',
        'maraude.jpeg',
        'aneth.jpeg',
        'nichoirs.jpg',
        'sarbacane.jpeg',
    ];

    private readonly Generator $faker;

    public function __construct(private readonly UploaderHelper $uploaderHelper)
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $loader = new NativeLoader();

        $objectSet = $loader->loadFiles(
            [
                __DIR__.'/ORM/fixtures/users.yaml',
                __DIR__.'/ORM/fixtures/partners.yaml',
                __DIR__.'/ORM/fixtures/thematics.yaml',
                __DIR__.'/ORM/fixtures/actions.yaml',
                __DIR__.'/ORM/fixtures/addresses.yaml',
                __DIR__.'/ORM/fixtures/occurrences.yaml',
            ]
        );

        foreach ($objectSet->getObjects() as $object) {
            $manager->persist($object);

            if ($object instanceof Action) {
                $randomImageFilename = $this->faker->randomElement(self::$actionPictures);
                $image = new File(__DIR__.'/images/action_picture/'.$randomImageFilename);
                $object->setPictureFilename($this->uploaderHelper->uploadPictureAction($image, $object->getPictureFilename()));
            }
        }
        $manager->flush();
    }
}
