<?php

namespace App\Listener;

use App\Event\UserRegistrationEvent;
use App\Service\SendGrid;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SendGridSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly SendGrid $sendGrid)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            UserRegistrationEvent::class => 'onRegistrationSuccess',
        ];
    }

    public function onRegistrationSuccess(UserRegistrationEvent $event)
    {
        $this->sendGrid->subscribeRegisteredUser($event->getUser()->getEmail());
    }
}
