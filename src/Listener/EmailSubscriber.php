<?php

namespace App\Listener;

use App\Event\NewsletterRegistrationEvent;
use App\Event\Registration\EditOccurrenceRegistrationEvent;
use App\Event\Registration\OccurrenceRegistrationNewEvent;
use App\Event\UserRegistrationEvent;
use App\Event\UserUploadParentalConsentEvent;
use App\Repository\UserRepository;
use App\Service\Mailer\ActionMailer;
use App\Service\Mailer\UserMailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly UserMailer $mailer, private readonly ActionMailer $actionMailer, private readonly UserRepository $userRepository)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            UserRegistrationEvent::class => 'onRegistrationSuccess',
            NewsletterRegistrationEvent::class => 'onNewsletterRegistration',
            OccurrenceRegistrationNewEvent::class => 'onRegistrationNew',
            EditOccurrenceRegistrationEvent::class => 'onOccurrenceRegistrationEdition',
            UserUploadParentalConsentEvent::class => 'onUploadParentalConsent',
        ];
    }

    public function onNewsletterRegistration(NewsletterRegistrationEvent $event)
    {
        $this->mailer->sendConfirmationNewsletterSubscription($event->getEmail());
    }

    public function onRegistrationSuccess(UserRegistrationEvent $event)
    {
        $this->mailer->sendUserRegistrationConfirmation($event->getUser());
    }

    public function onRegistrationNew(OccurrenceRegistrationNewEvent $event)
    {
        $registration = $event->getRegistration();

        $this->actionMailer->sendOnNewActionRegistrationToVolunteer($registration);

        foreach ($registration->getOccurrence()->getReferrers() as $referrer) {
            $this->actionMailer->sendOnNewActionRegistrationToReferrer($event->getRegistration(), $referrer);
        }
    }

    public function onOccurrenceRegistrationEdition(EditOccurrenceRegistrationEvent $event)
    {
        $registration = $event->getRegistration();

        $this->actionMailer->sendOnOccurrenceRegistrationEditionToVolunteer($registration);

        foreach ($registration->getOccurrence()->getReferrers() as $referrer) {
            $this->actionMailer->sendOnOccurrenceRegistrationEditionToReferrer($event->getRegistration(), $referrer);
        }
    }

    public function onUploadParentalConsent(UserUploadParentalConsentEvent $event)
    {
        $user = $event->getUser();
        $admins = $this->userRepository->findAllByRole('ROLE_ADMIN');

        foreach ($admins as $admin) {
            $this->actionMailer->sendOnParentalConsentUploadToAdmin($admin, $user);
        }
    }
}
