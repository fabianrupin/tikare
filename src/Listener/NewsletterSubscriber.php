<?php

namespace App\Listener;

use App\Event\UserEditionEvent;
use App\Event\UserRegistrationEvent;
use App\Service\Newsletter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NewsletterSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly Newsletter $newsletter)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            UserRegistrationEvent::class => 'onRegistrationSuccess',
            UserEditionEvent::class => 'onEdition',
        ];
    }

    public function onRegistrationSuccess(UserRegistrationEvent $event)
    {
        $this->newsletter->subscribe($event->getUser()->getEmail());
    }

    public function onEdition(UserEditionEvent $event)
    {
        if ($event->getUser()->isNewsletter()) {
            $this->newsletter->subscribe($event->getUser()->getEmail());

            return;
        }
        $this->newsletter->unsubscribe($event->getUser()->getEmail());
    }
}
