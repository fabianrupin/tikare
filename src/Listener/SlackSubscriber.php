<?php

namespace App\Listener;

use App\Event\NewsletterRegistrationEvent;
use App\Event\Registration\OccurrenceRegistrationCancelEvent;
use App\Event\Registration\OccurrenceRegistrationNewEvent;
use App\Event\UserRegistrationEvent;
use App\Service\Chatter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class SlackSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly Chatter $hook)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            ResponseEvent::class => 'onResponse',
            UserRegistrationEvent::class => 'onUserRegistrationSuccess',
            NewsletterRegistrationEvent::class => 'onNewsletterRegistration',
            OccurrenceRegistrationNewEvent::class => 'onActionRegistration',
            OccurrenceRegistrationCancelEvent::class => 'onActionCancellation',
        ];
    }

    public function onResponse(ResponseEvent $event)
    {
        /*        if ($event->getResponse()->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
                    $this->hook->send('TiKare a rencontré une erreur serveur');
                }*/
    }

    public function onUserRegistrationSuccess(UserRegistrationEvent $event)
    {
        $this->hook->sendUserRegistrationConfirmation($event->getUser());
    }

    public function onNewsletterRegistration(NewsletterRegistrationEvent $event)
    {
        $this->hook->sendNewsletterSubscriptionHook($event->getEmail());
    }

    public function onActionRegistration(OccurrenceRegistrationNewEvent $event)
    {
        $this->hook->sendActionRegistrationHook($event->getRegistration());
    }

    public function onActionCancellation(OccurrenceRegistrationCancelEvent $event)
    {
        $this->hook->sendActionCancellationHook($event->getRegistration());
    }
}
