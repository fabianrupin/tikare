<?php

namespace App\Controller\Referrer;

use App\Entity\Occurrence;
use App\Entity\User;
use App\Repository\OccurrenceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/referent/occurrence')]
class OccurrenceController extends AbstractController
{
    final public const MAX_PER_PAGE = 20;

    public function __construct(private readonly OccurrenceRepository $repository, private readonly PaginatorInterface $paginator)
    {
    }

    #[Route(path: '/liste/{page}', name: 'referrer_occurrence_index', methods: ['GET'], defaults: ['page' => 1])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        /** @var User $user */
        $user = $this->getUser();
        $qb = $this->repository->findAllByReferrerQueryBuilder($user, $term);
        $occurrences = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('referrer/occurrence/index.html.twig', [
            'occurrences' => $occurrences,
        ]);
    }

    #[Route(path: '/sans-referents/liste/{page}', name: 'referrer_occurrence_index_without_referrer', methods: ['GET'], defaults: ['page' => 1])]
    public function indexWithoutReferrer(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllWithoutReferrer($term);
        $occurrences = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('referrer/occurrence/index.html.twig', [
            'occurrences' => $occurrences,
        ]);
    }

    #[Route(path: '/{id}', name: 'referrer_occurrence_show', methods: ['GET'])]
    public function show(Occurrence $occurrence): Response
    {
        return $this->render('referrer/occurrence/show.html.twig', [
            'occurrence' => $occurrence,
        ]);
    }
}
