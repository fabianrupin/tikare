<?php

namespace App\Controller\Referrer;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/referent/utilisateurs')]
class UserController extends AbstractController
{
    public function __construct(private readonly UserRepository $repository)
    {
    }

    #[Route(path: '/{id}', name: 'referrer_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('referrer/user/show.html.twig', [
            'user' => $user,
        ]);
    }
}
