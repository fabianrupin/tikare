<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Admin\User\AdminUserCreateType;
use App\Form\Admin\User\AdminUserEditType;
use App\Form\User\UploadParentalConsentType;
use App\Model\DTO\Admin\User\AdminCreateUserDTO;
use App\Model\DTO\Admin\User\AdminEditUserDTO;
use App\Model\DTO\User\UploadParentalConsentDTO;
use App\Repository\UserRepository;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/utilisateurs')]
class UserController extends AbstractController
{
    final public const MAX_PER_PAGE = 10;

    public function __construct(
        private readonly UserRepository $repository,
        private readonly UploaderHelper $uploaderHelper,
        private readonly PaginatorInterface $paginator,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route(path: '/liste/{page}', name: 'admin_user_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllQueryBuilder($term);
        $users = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route(path: '/creer', name: 'admin_user_new', methods: ['GET', 'POST'])]
    public function create(Request $request): Response
    {
        $userDTO = new AdminCreateUserDTO();
        $form = $this->createForm(AdminUserCreateType::class, $userDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = User::createFromAdmin($userDTO);

            $this->repository->save($user);

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/new.html.twig', [
            'user' => $userDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route(path: '/{id}/modifier', name: 'admin_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user): Response
    {
        $userDTO = new AdminEditUserDTO($user);
        $form = $this->createForm(AdminUserEditType::class, $userDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->edit($userDTO);

            if ($uploadedFile = $userDTO->getProfilePicture()) {
                $profilePictureFilename = $this->uploaderHelper->uploadPictureProfile($uploadedFile, $user->getProfilePictureFilename());
                $user->setProfilePictureFilename($profilePictureFilename);
            }

            $this->repository->save($user);
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}/desactiver', name: 'admin_user_deactivate', methods: ['GET', 'POST'])]
    public function deactivate(User $user): Response
    {
        $user->disable();
        $this->repository->save($user);

        return $this->redirectToRoute('admin_user_show', [
            'id' => $user->getId(),
        ]);
    }

    #[Route(path: '/{id}/activer', name: 'admin_user_activate', methods: ['GET', 'POST'])]
    public function activate(User $user): Response
    {
        $user->enable();
        $this->repository->save($user);

        return $this->redirectToRoute('admin_user_show', [
            'id' => $user->getId(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_user_delete', methods: ['DELETE'])]
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin_user_index');
    }

    #[Route(path: '/{id}/autorisation/valider', name: 'admin_parental_consent_validate', methods: ['GET', 'POST'])]
    public function parentalConsentValidate(User $user): Response
    {
        $user->parentalConsentValidate();
        $this->repository->save($user);

        return $this->redirectToRoute('admin_user_show', [
            'id' => $user->getId(),
        ]);
    }

    #[Route(path: '/{id}/autorisation/téléverser', name: 'admin_upload_parental_consent', methods: ['GET', 'POST'])]
    public function uploadParentalConsent(User $user, Request $request): Response
    {
        $parentalConsentDTO = new UploadParentalConsentDTO();
        $form = $this->createForm(UploadParentalConsentType::class, $parentalConsentDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parentalConsentFilename = $this->uploaderHelper->uploadParentalConsent($parentalConsentDTO->getParentalConsentFile());
            $user->setParentalConsentFilename($parentalConsentFilename);

            $this->repository->save($user);

            $this->addFlash('success', 'user.upload_parental_consent.success');

            return $this->redirectToRoute('admin_user_show', [
                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/parental_consent.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}/autorisation/télécharger', name: 'admin_download_parental_consent', methods: ['GET'])]
    public function downloadParentalConsent(User $user, UploaderHelper $uploaderHelper): Response
    {
        if (!$parentalConsentFile = $user->getParentalConsentFile()) {
            $this->addFlash('success', 'user.download_parental_consent.failed');

            return $this->redirectToRoute('admin_user_show', ['id' => $user->getId()]);
        }
        $response = new StreamedResponse(function () use ($parentalConsentFile, $uploaderHelper) {
            $outputStream = fopen('php://output', 'wb');
            $fileStream = $uploaderHelper->readStream($parentalConsentFile->getFilePath(), false);

            stream_copy_to_stream($fileStream, $outputStream);
        });
        $response->headers->set('Content-Type', $parentalConsentFile->getMimeType());
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $parentalConsentFile->getOriginalFilename()
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
