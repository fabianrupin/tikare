<?php

namespace App\Controller\Admin;

use App\Entity\Registration;
use App\Form\Admin\Registration\RegistrationType;
use App\Model\DTO\Admin\Registration\AdminRegistrationDTO;
use App\Repository\RegistrationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/registration')]
class RegistrationController extends AbstractController
{
    final public const MAX_PER_PAGE = 20;

    public function __construct(
        private readonly RegistrationRepository $repository,
        private readonly PaginatorInterface $paginator,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    #[Route(path: '/liste/{page}', name: 'admin_registration_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllQueryBuilder($term);
        $registrations = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('admin/registration/index.html.twig', [
            'registrations' => $registrations,
        ]);
    }

    #[Route(path: '/creer', name: 'admin_registration_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $registrationDTO = new AdminRegistrationDTO();
        $form = $this->createForm(RegistrationType::class, $registrationDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->repository->findBy([
                'volunteer' => $registrationDTO->getVolunteer(),
                'occurrence' => $registrationDTO->getOccurrence(),
            ])) {
                $this->addFlash('danger', 'registration.creation.forbidden');

                return $this->redirectToRoute('occurrence_index');
            }

            $registration = Registration::createFromAdmin($registrationDTO);
            $this->repository->save($registration);

            return $this->redirectToRoute('registration_index');
        }

        return $this->render('admin/registration/new.html.twig', [
            'registration' => $registrationDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_registration_show', methods: ['GET'])]
    public function show(Registration $registration): Response
    {
        return $this->render('admin/registration/show.html.twig', [
            'registration' => $registration,
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_registration_delete', methods: ['DELETE'])]
    public function delete(Request $request, Registration $registration): Response
    {
        if ($this->isCsrfTokenValid('delete'.$registration->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($registration);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('registration_index');
    }
}
