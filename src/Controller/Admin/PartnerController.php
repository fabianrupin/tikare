<?php

namespace App\Controller\Admin;

use App\Entity\Partner;
use App\Form\Admin\Partner\PartnerType;
use App\Model\DTO\Admin\Partner\AdminCreatePartnerDTO;
use App\Model\DTO\Admin\Partner\AdminEditPartnerDTO;
use App\Repository\PartnerRepository;
use App\Service\AirTable;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/partenaires')]
class PartnerController extends AbstractController
{
    final public const MAX_PER_PAGE = 10;

    public function __construct(
        private readonly PartnerRepository $repository,
        private readonly UploaderHelper $uploaderHelper,
        private readonly PaginatorInterface $paginator,
        private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/liste/{page}', name: 'admin_partner_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllQueryBuilder($term);
        $partners = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('admin/partner/index.html.twig', [
            'partners' => $partners,
        ]);
    }

    #[Route(path: '/creer/{airtableId}', name: 'admin_partner_new', methods: ['GET', 'POST'])]
    public function create(Request $request, AirTable $airTable, ?string $airtableId = null): Response
    {
        $partnerDTO = new AdminCreatePartnerDTO();
        if ($airtableId) {
            $partner = $airTable->getPartnerInformation($airtableId)->toArray();
            $partnerDTO->setAirTableId($partner['id']);
            $partnerDTO->setName($partner['fields']['Name']);
            if (array_key_exists('Site internet', $partner['fields'])) {
                $partnerDTO->setWebsite($partner['fields']['Site internet']);
            }
        }
        $form = $this->createForm(PartnerType::class, $partnerDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $partner = Partner::create($partnerDTO);

            if ($uploadedFile = $partnerDTO->getLogo()) {
                $logoFilename = $this->uploaderHelper->uploadLogoPartner($uploadedFile, $partner->getLogoFilename());
                $partner->setLogoFilename($logoFilename);
            }

            $this->repository->save($partner);

            return $this->redirectToRoute('admin_partner_index');
        }

        return $this->render('admin/partner/new.html.twig', [
            'partner' => $partnerDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_partner_show', methods: ['GET'])]
    public function show(Partner $partner): Response
    {
        return $this->render('admin/partner/show.html.twig', [
            'partner' => $partner,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'admin_partner_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Partner $partner): Response
    {
        $partnerDTO = new AdminEditPartnerDTO($partner);
        $form = $this->createForm(PartnerType::class, $partnerDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $partner->edit($partnerDTO);

            if ($uploadedFile = $partnerDTO->getLogo()) {
                $logoFilename = $this->uploaderHelper->uploadLogoPartner($uploadedFile, $partner->getLogoFilename());
                $partner->setLogoFilename($logoFilename);
            }

            $this->repository->save($partner);

            return $this->redirectToRoute('admin_partner_index');
        }

        return $this->render('admin/partner/edit.html.twig', [
            'partner' => $partner,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_partner_delete', methods: ['DELETE'])]
    public function delete(Request $request, Partner $partner): Response
    {
        if ($this->isCsrfTokenValid('delete'.$partner->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($partner);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin_partner_index');
    }
}
