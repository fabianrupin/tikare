<?php

namespace App\Controller\Admin;

use App\Entity\Action;
use App\Form\Admin\Action\ActionType;
use App\Model\DTO\Admin\Action\AdminActionDTO;
use App\Repository\ActionRepository;
use App\Service\AirTable;
use App\Service\UploaderHelper;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/action')]
class ActionController extends AbstractController
{
    final public const MAX_PER_PAGE = 10;

    public function __construct(
        private readonly ActionRepository $repository,
        private readonly UploaderHelper $uploaderHelper,
        private readonly PaginatorInterface $paginator,
        private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/liste/{page}', name: 'admin_action_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllQueryBuilder($term);
        $actions = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('admin/action/index.html.twig', [
            'actions' => $actions,
        ]);
    }

    #[Route(path: '/creer/{airtableId}', name: 'admin_action_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AirTable $airTable, ?string $airtableId = null): Response
    {
        $actionDTO = new AdminActionDTO();
        if ($airtableId) {
            $action = $airTable->getActionInformation($airtableId)->toArray();
            $actionDTO->setAirTableId($action['id']);
            $actionDTO->setTitle($action['fields']['Name']);
            if (array_key_exists('Contenu', $action['fields'])) {
                $actionDTO->setDescription($action['fields']['Contenu']);
            }
            if (array_key_exists('Contact partenaire', $action['fields'])) {
                $people = $airTable->getPeopleInformation($action['fields']['Contact partenaire'][0])->toArray();
                if (array_key_exists('PRENOM', $people['fields'])) {
                    $actionDTO->setContactFirstName($people['fields']['PRENOM']);
                }
                if (array_key_exists('NOM', $people['fields'])) {
                    $actionDTO->setContactLastName($people['fields']['NOM']);
                }
                if (array_key_exists('MAIL', $people['fields'])) {
                    $actionDTO->setContactEmail($people['fields']['MAIL']);
                }
                if (array_key_exists('TELEPHONE PORTABLE', $people['fields'])) {
                    $actionDTO->setContactPhoneNumber($people['fields']['TELEPHONE PORTABLE']);
                }
                if (array_key_exists('FONCTION', $people['fields'])) {
                    $actionDTO->setContactRole($people['fields']['FONCTION'][0]);
                }
            }
        }
        $form = $this->createForm(ActionType::class, $actionDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $action = Action::create($actionDTO);

            if ($uploadedFile = $actionDTO->getPicture()) {
                $logoFilename = $this->uploaderHelper->uploadPictureAction($uploadedFile, $action->getPictureFilename());
                $action->setPictureFilename($logoFilename);
            }

            $this->repository->save($action);

            return $this->redirectToRoute('admin_action_index');
        }

        return $this->render('admin/action/new.html.twig', [
            'action' => $actionDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_action_show', methods: ['GET'])]
    public function show(Action $action): Response
    {
        return $this->render('admin/action/show.html.twig', [
            'action' => $action,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'admin_action_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Action $action): Response
    {
        $actionDTO = new AdminActionDTO($action);
        $form = $this->createForm(ActionType::class, $actionDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $action->edit($actionDTO);

            if ($uploadedFile = $actionDTO->getPicture()) {
                $logoFilename = $this->uploaderHelper->uploadPictureAction($uploadedFile, $action->getPictureFilename());
                $action->setPictureFilename($logoFilename);
            }

            $this->repository->save($action);

            return $this->redirectToRoute('admin_action_index');
        }

        return $this->render('admin/action/edit.html.twig', [
            'action' => $action,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_action_delete', methods: ['DELETE'])]
    public function delete(Request $request, Action $action): Response
    {
        if ($this->isCsrfTokenValid('delete'.$action->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($action);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin_action_index');
    }
}
