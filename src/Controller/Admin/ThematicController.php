<?php

namespace App\Controller\Admin;

use App\Entity\Thematic;
use App\Form\Admin\Thematic\ThematicType;
use App\Repository\ThematicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/thematic')]
class ThematicController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/', name: 'admin_thematic_index', methods: ['GET'])]
    public function index(ThematicRepository $thematicRepository): Response
    {
        return $this->render('admin/thematic/index.html.twig', [
            'thematics' => $thematicRepository->findAll(),
        ]);
    }

    #[Route(path: '/creer', name: 'admin_thematic_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $thematic = new Thematic();
        $form = $this->createForm(ThematicType::class, $thematic);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($thematic);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_thematic_index');
        }

        return $this->render('admin/thematic/new.html.twig', [
            'thematic' => $thematic,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_thematic_show', methods: ['GET'])]
    public function show(Thematic $thematic): Response
    {
        return $this->render('admin/thematic/show.html.twig', [
            'thematic' => $thematic,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'admin_thematic_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Thematic $thematic): Response
    {
        $form = $this->createForm(ThematicType::class, $thematic);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_thematic_index');
        }

        return $this->render('admin/thematic/edit.html.twig', [
            'thematic' => $thematic,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_thematic_delete', methods: ['DELETE'])]
    public function delete(Request $request, Thematic $thematic): Response
    {
        if ($this->isCsrfTokenValid('delete'.$thematic->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($thematic);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin_thematic_index');
    }
}
