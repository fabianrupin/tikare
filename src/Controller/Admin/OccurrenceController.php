<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use App\Entity\Occurrence;
use App\Form\Admin\Occurrence\OccurrenceType;
use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;
use App\Repository\AddressRepository;
use App\Repository\OccurrenceRepository;
use App\Service\Mailer\ActionMailer;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/occurrence')]
class OccurrenceController extends AbstractController
{
    final public const MAX_PER_PAGE = 20;

    public function __construct(
        private readonly OccurrenceRepository $repository,
        private readonly AddressRepository $addressRepository,
        private readonly PaginatorInterface $paginator,
        private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/liste/{page}', name: 'admin_occurrence_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllQueryBuilder($term);
        $occurrences = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('admin/occurrence/index.html.twig', [
            'occurrences' => $occurrences,
        ]);
    }

    #[Route(path: '/new', name: 'admin_occurrence_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $occurrenceDTO = new AdminOccurrenceDTO();
        $form = $this->createForm(OccurrenceType::class, $occurrenceDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $address = Address::create($occurrenceDTO);
            $this->addressRepository->save($address);

            $occurrence = Occurrence::create($occurrenceDTO, $address);
            $this->repository->save($occurrence);

            return $this->redirectToRoute('admin_occurrence_index');
        }

        return $this->render('admin/occurrence/new.html.twig', [
            'occurrence' => $occurrenceDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}', name: 'admin_occurrence_show', methods: ['GET'])]
    public function show(Occurrence $occurrence): Response
    {
        return $this->render('admin/occurrence/show.html.twig', [
            'occurrence' => $occurrence,
        ]);
    }

    #[Route(path: '/{id}/edit', name: 'admin_occurrence_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Occurrence $occurrence): Response
    {
        $occurrenceDTO = new AdminOccurrenceDTO($occurrence);
        $form = $this->createForm(OccurrenceType::class, $occurrenceDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $occurrence->getAddress()->edit($occurrenceDTO);
            $this->addressRepository->save($occurrence->getAddress());

            $occurrence->edit($occurrenceDTO);
            $this->repository->save($occurrence);

            return $this->redirectToRoute('admin_occurrence_index');
        }

        return $this->render('admin/occurrence/edit.html.twig', [
            'occurrence' => $occurrence,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}/annuler', name: 'admin_occurrence_cancel', methods: ['GET'])]
    public function cancel(Occurrence $occurrence, ActionMailer $mailer): Response
    {
        $occurrence->cancel();
        $this->repository->save($occurrence);
        $mailer->sendOnOccurrenceCancellation($occurrence);

        return $this->redirectToRoute('admin_occurrence_index');
    }

    #[Route(path: '/{id}', name: 'admin_occurrence_delete', methods: ['DELETE'])]
    public function delete(Request $request, Occurrence $occurrence): Response
    {
        if ($this->isCsrfTokenValid('delete'.$occurrence->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($occurrence);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin_occurrence_index');
    }
}
