<?php

namespace App\Controller\Admin;

use App\Service\AirTable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/admin/airtable')]
class UtilityController extends AbstractController
{
    public function __construct(private readonly AirTable $airTable)
    {
    }

    #[Route(path: '/partenaire', name: 'admin_partner_airtable_index', methods: ['GET'])]
    public function getPartnersListFromAirtable(): Response
    {
        $partners = $this->airTable->getPartnersList()->toArray()['records'];

        return $this->render('admin/partner/airtable_index.html.twig', [
            'partners' => $partners,
        ]);
    }

    #[Route(path: '/partenaire/{id}', name: 'admin_partner_airtable_show', methods: ['GET'])]
    public function getPartnerFromAirtable(string $id): JsonResponse
    {
        $partner = $this->airTable->getPartnerInformation($id)->toArray();

        return $this->json($partner);
    }

    #[Route(path: '/action', name: 'admin_action_airtable_index', methods: ['GET'])]
    public function getActionsListFromAirtable(): Response
    {
        $actions = $this->airTable->getActionsList()->toArray()['records'];
        /*        dd($actions); */
        return $this->render('admin/action/airtable_index.html.twig', [
            'actions' => $actions,
        ]);
    }

    #[Route(path: '/action/{id}', name: 'admin_action_airtable_show', methods: ['GET'])]
    public function getActionFromAirtable(string $id): JsonResponse
    {
        $partner = $this->airTable->getActionInformation($id)->toArray();

        return $this->json($partner);
    }
}
