<?php

namespace App\Controller\User;

use App\Entity\ChangeEmailRequest;
use App\Entity\User;
use App\Form\User\ChangeEmailFormType;
use App\Model\DTO\User\ChangeEmailDTO;
use App\Repository\ChangeEmailRequestRepository;
use App\Security\EmailVerifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

#[Route(path: '/modification-email')]
class ChangeEmailController extends AbstractController
{
    public function __construct(private readonly EmailVerifier $emailVerifier, private readonly ChangeEmailRequestRepository $repository)
    {
    }

    #[Route(path: '', name: 'app_change_email_request')]
    public function request(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            return $this->redirectToRoute('homepage');
        }

        if ($changeEmailRequest = $user->getChangeEmailRequest()) {
            $this->repository->remove($changeEmailRequest);
        }
        $changeEmailDTO = new ChangeEmailDTO();
        $form = $this->createForm(ChangeEmailFormType::class, $changeEmailDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $changeEmailRequest = ChangeEmailRequest::create($changeEmailDTO, $user);

            $this->repository->save($changeEmailRequest);

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $changeEmailRequest);

            $this->addFlash('success', 'user.change_email.success ');

            return $this->redirectToRoute('user_show');
        }

        return $this->render('user/change_email.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/verification', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('homepage');
        }
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('user_show');
        }
        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'user.confirm_email.success');

        return $this->redirectToRoute('user_show');
    }
}
