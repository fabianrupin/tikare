<?php

namespace App\Controller\User;

use App\Entity\ParentalConsentFile;
use App\Entity\User;
use App\Event\UserEditionEvent;
use App\Event\UserUploadParentalConsentEvent;
use App\Form\User\UploadParentalConsentType;
use App\Form\User\UserEditType;
use App\Model\DTO\User\EditUserDTO;
use App\Model\DTO\User\UploadParentalConsentDTO;
use App\Repository\UserRepository;
use App\Service\UploaderHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/profil')]
class UserController extends AbstractController
{
    public function __construct(private readonly EventDispatcherInterface $eventDispatcher, private readonly UploaderHelper $uploaderHelper, private readonly UserRepository $repository)
    {
    }

    #[Route(path: '', name: 'user_show', methods: ['GET'])]
    public function show(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route(path: '/public/{id}', name: 'user_public_show', methods: ['GET'])]
    public function publicShow(User $user): Response
    {
        if ($user->isDeactivated() || !$user->isEnabled()) {
            throw new NotFoundHttpException();
        }

        return $this->render('user/public_show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route(path: '/modifier', name: 'user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request): Response
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->redirectToRoute('homepage');
        }
        $user instanceof User ?:
        $userDTO = new EditUserDTO($user);
        $form = $this->createForm(UserEditType::class, $userDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->edit($userDTO);

            if ($uploadedFile = $userDTO->getProfilePicture()) {
                $profilePictureFilename = $this->uploaderHelper->uploadPictureProfile($uploadedFile, $user->getProfilePictureFilename());
                $user->setProfilePictureFilename($profilePictureFilename);
            }

            $this->repository->save($user);

            $this->eventDispatcher->dispatch(new UserEditionEvent($user));

            $this->addFlash('success', 'user.modification.success');

            return $this->redirectToRoute('user_show');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/televerser-mon-autorisation-parentale', name: 'user_upload_parental_consent', methods: ['GET', 'POST'])]
    public function uploadParentalConsent(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $parentalConsentDTO = new UploadParentalConsentDTO();
        $form = $this->createForm(UploadParentalConsentType::class, $parentalConsentDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $parentalConsentFilename = $this->uploaderHelper->uploadParentalConsent($parentalConsentDTO->getParentalConsentFile());

            $parentalConsentFile = ParentalConsentFile::create($parentalConsentDTO, $parentalConsentFilename);

            $user->setParentalConsentFile($parentalConsentFile);

            $this->repository->save($user);

            $this->eventDispatcher->dispatch(new UserUploadParentalConsentEvent($user));

            $this->addFlash('success', 'user.upload_parental_consent.success');

            return $this->redirectToRoute('user_show');
        }

        return $this->render('user/parental_consent.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/télécharger-autorisation-parental', name: 'user_download_parental_consent', methods: ['GET'])]
    public function downloadParentalConsent(UploaderHelper $uploaderHelper)
    {
        /** @var User $user */
        $user = $this->getUser();
        $parentalConsentFile = $user->getParentalConsentFile();
        $response = new StreamedResponse(function () use ($parentalConsentFile, $uploaderHelper) {
            $outputStream = fopen('php://output', 'wb');
            $fileStream = $uploaderHelper->readStream($parentalConsentFile->getFilePath(), false);

            stream_copy_to_stream($fileStream, $outputStream);
        });
        $response->headers->set('Content-Type', $parentalConsentFile->getMimeType());
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $parentalConsentFile->getOriginalFilename()
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    #[Route(path: '/désactiver-mon-compte', name: 'user_deactivate', methods: ['GET'])]
    public function deactivate(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $user->deactivate();
        $this->repository->save($user);

        return $this->redirectToRoute('app_logout');
    }

    #[Route(path: '/supprimer-mon-compte/{id}', name: 'user_delete', methods: ['GET'])]
    public function delete(Request $request, User $user): Response
    {
        // Todo
        /*        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
                      $entityManager = $this->getDoctrine()->getManager();
                      $entityManager->remove($user);
                      $entityManager->flush();
                  }*/
        return $this->redirectToRoute('app_logout');
    }
}
