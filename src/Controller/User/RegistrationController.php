<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Event\UserRegistrationEvent;
use App\Form\User\UserRegistrationType;
use App\Model\DTO\User\UserRegistrationDTO;
use App\Repository\UserRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegistrationController extends AbstractController
{
    #[Route(path: '/l-inscription', name: 'register')]
    public function registration(Request $request, UserRepository $userRepository, EventDispatcherInterface $eventDispatcher): Response
    {
        $userRegistrationDTO = new UserRegistrationDTO();
        $form = $this->createForm(UserRegistrationType::class, $userRegistrationDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = User::create($userRegistrationDTO);

            $userRepository->save($user);

            $eventDispatcher->dispatch(new UserRegistrationEvent($user));

            $this->addFlash('success', 'user.registration.success');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('user/registration.html.twig', [
            'user_register_form' => $form->createView(),
        ]);
    }
}
