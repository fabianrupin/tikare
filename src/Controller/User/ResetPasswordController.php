<?php

namespace App\Controller\User;

use App\Entity\User;
use App\Form\User\ChangePasswordFormType;
use App\Form\User\ResetPasswordRequestFormType;
use App\Model\DTO\User\ChangePasswordDTO;
use App\Model\DTO\User\ResetPasswordRequestDTO;
use App\Repository\UserRepository;
use App\Service\Mailer\UserMailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

#[Route(path: '/modification-du-mot-de-passe')]
class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    public function __construct(
        private readonly ResetPasswordHelperInterface $resetPasswordHelper,
        private readonly UserMailer $mailer,
        private readonly UserRepository $userRepository)
    {
    }

    #[Route(path: '', name: 'app_forgot_password_request')]
    public function request(Request $request): Response
    {
        $resetPasswordRequestDTO = new ResetPasswordRequestDTO();
        $form = $this->createForm(ResetPasswordRequestFormType::class, $resetPasswordRequestDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSendingPasswordResetEmail(
                $resetPasswordRequestDTO,
                $this->mailer
            );
        }

        return $this->render('reset_password/request.html.twig', [
            'requestForm' => $form->createView(),
        ]);
    }

    /**
     * Confirmation page after a user has requested a password reset.
     */
    #[Route(path: '/confirmation', name: 'app_check_email')]
    public function checkEmail(): Response
    {
        if (!$this->canCheckEmail()) {
            return $this->redirectToRoute('app_forgot_password_request');
        }

        return $this->render('reset_password/check_email.html.twig', [
            'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
        ]);
    }

    #[Route(path: '/modification/{token}', name: 'app_reset_password')]
    public function reset(Request $request, string $token = null): Response
    {
        if ($token) {
            // We store the token in session and remove it from the URL, to avoid the URL being
            // loaded in a browser and potentially leaking the token to 3rd party JavaScript.
            $this->storeTokenInSession($token);

            return $this->redirectToRoute('app_reset_password');
        }
        $token = $this->getTokenFromSession();
        if (null === $token) {
            throw $this->createNotFoundException('No reset password token found in the URL or in the session.');
        }
        try {
            /** @var User $user */
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('reset_password_error', sprintf(
                'There was a problem validating your reset request - %s',
                $e->getReason()
            ));

            return $this->redirectToRoute('app_forgot_password_request');
        }
        $changePasswordDTO = new ChangePasswordDTO();
        $form = $this->createForm(ChangePasswordFormType::class, $changePasswordDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->resetPasswordHelper->removeResetRequest($token);

            $user->changePassword($changePasswordDTO);

            $this->userRepository->save($user);

            $this->cleanSessionAfterReset();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('reset_password/reset.html.twig', [
            'resetForm' => $form->createView(),
        ]);
    }

    private function processSendingPasswordResetEmail(
        ResetPasswordRequestDTO $resetPasswordRequestDTO,
        UserMailer $mailer,
        UserRepository $userRepository
    ): RedirectResponse {
        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => $resetPasswordRequestDTO->getEmail(),
        ]);

        $this->setCanCheckEmailInSession();

        if (!$user) {
            return $this->redirectToRoute('app_check_email');
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            $this->addFlash('reset_password_error', sprintf(
                'There was a problem handling your password reset request - %s',
                $e->getReason()
            ));

            return $this->redirectToRoute('app_forgot_password_request');
        }

        $mailer->sendPasswordResetRequest($user, $resetToken, $this->resetPasswordHelper->getTokenLifetime());

        return $this->redirectToRoute('app_check_email');
    }
}
