<?php

namespace App\Controller;

use App\Event\NewsletterRegistrationEvent;
use App\Form\ContactFormType;
use App\Form\NewsletterSubscriptionFormType;
use App\Model\DTO\Homepage\ContactFormDTO;
use App\Model\DTO\Homepage\NewsletterSubscriptionFormDTO;
use App\Repository\OccurrenceRepository;
use App\Service\Mailer\UserMailer;
use App\Service\Newsletter;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    #[Route(path: '', name: 'homepage')]
    public function index(Request $request, UserMailer $mailer, Newsletter $newsletter, EventDispatcherInterface $eventDispatcher, OccurrenceRepository $repository): Response
    {
        $contactFormDTO = new ContactFormDTO();
        $contactForm = $this->createForm(ContactFormType::class, $contactFormDTO);
        $newsletterSubscriptionFormDTO = new NewsletterSubscriptionFormDTO();
        $newsletterSubscriptionForm = $this->createForm(NewsletterSubscriptionFormType::class, $newsletterSubscriptionFormDTO);
        $newsletterSubscriptionForm->handleRequest($request);
        $contactForm->handleRequest($request);
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $mailer->sendContactMessage($contactFormDTO);

            $this->addFlash('success', 'contact-form.success');

            return $this->redirectToRoute('homepage');
        }
        if ($newsletterSubscriptionForm->isSubmitted() && $newsletterSubscriptionForm->isValid()) {
            $email = $newsletterSubscriptionFormDTO->getEmail();
            $newsletter->subscribe($email);

            $this->addFlash('success', 'newsletter-subscription.success');

            $eventDispatcher->dispatch(new NewsletterRegistrationEvent($email));

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage/index.html.twig', [
            'contactForm' => $contactForm->createView(),
            'newsletterSubscriptionFrom' => $newsletterSubscriptionForm->createView(),
            'occurrences' => $repository->findTheNext(3),
        ]);
    }
}
