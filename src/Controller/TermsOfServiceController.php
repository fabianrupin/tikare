<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TermsOfServiceController extends AbstractController
{
    #[Route(path: '/CGU', name: 'terms_of_service')]
    public function show(): Response
    {
        return $this->render('terms_of_service/index.html.twig');
    }
}
