<?php

namespace App\Controller;

use App\Form\ContactFormType;
use App\Model\DTO\Homepage\ContactFormDTO;
use App\Service\Mailer\UserMailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactUsController extends AbstractController
{
    #[Route(path: '/contactez-nous', name: 'contact_us')]
    public function index(Request $request, UserMailer $mailer): Response
    {
        $contactFormDTO = new ContactFormDTO();
        $contactForm = $this->createForm(ContactFormType::class, $contactFormDTO);
        $contactForm->handleRequest($request);
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            $mailer->sendContactMessage($contactFormDTO);

            $this->addFlash('success', 'contact-form.success');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('contact_us/index.html.twig', [
            'contactForm' => $contactForm->createView(),
        ]);
    }
}
