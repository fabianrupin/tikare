<?php

namespace App\Controller\Calendar;

use App\Entity\Occurrence;
use App\Entity\Registration;
use App\Entity\User;
use App\Event\Registration\EditOccurrenceRegistrationEvent;
use App\Event\Registration\OccurrenceRegistrationCancelEvent;
use App\Event\Registration\OccurrenceRegistrationNewEvent;
use App\Form\Calendar\Registration\RegistrationType;
use App\Model\DTO\Calendar\Registration\RegistrationDTO;
use App\Repository\RegistrationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/inscription')]
#[IsGranted('ROLE_USER')]
class RegistrationController extends AbstractController
{
    final public const MAX_PER_PAGE = 20;

    public function __construct(private readonly RegistrationRepository $repository, private readonly PaginatorInterface $paginator, private readonly EventDispatcherInterface $eventDispatcher)
    {
    }

    #[Route(path: '/liste/{page}', name: 'registration_index', defaults: ['page' => 1], methods: ['GET'])]
    public function index(Request $request, string $page): Response
    {
        $term = $request->get('search');
        $qb = $this->repository->findAllByUserQueryBuilder($this->getUser(), $term);
        $registrations = $this->paginator->paginate($qb, (int) $page, self::MAX_PER_PAGE);

        return $this->render('calendar/registration/index.html.twig', [
            'registrations' => $registrations,
        ]);
    }

    #[Route(path: '/occurrence/{id}', name: 'registration_new', methods: ['GET', 'POST'])]
    public function new(Request $request, Occurrence $occurrence): Response
    {
        if (!$occurrence->isOpen()) {
            $this->addFlash('danger', 'registration.creation.not_open');

            return $this->redirectToRoute('occurrence_show', ['id' => $occurrence->getId()]);
        }
        $registrationDTO = new RegistrationDTO();
        $form = $this->createForm(RegistrationType::class, $registrationDTO);
        $form->handleRequest($request);
        if ($this->repository->findBy(['volunteer' => $this->getUser(), 'occurrence' => $occurrence])) {
            $this->addFlash('danger', 'registration.creation.forbidden');

            return $this->redirectToRoute('occurrence_index');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();

            $registration = Registration::create($registrationDTO, $occurrence, $user);

            $this->repository->save($registration);

            $this->eventDispatcher->dispatch(new OccurrenceRegistrationNewEvent($registration));

            $this->addFlash('success', 'registration.creation.success');

            return $this->redirectToRoute('registration_index');
        }

        return $this->render('calendar/registration/new.html.twig', [
            'occurrence' => $occurrence,
            'registration' => $registrationDTO,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}/modifier', name: 'registration_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Registration $registration): Response
    {
        $registrationDTO = new RegistrationDTO($registration);
        $form = $this->createForm(RegistrationType::class, $registrationDTO);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();

            $registration->edit($registrationDTO);

            $this->repository->save($registration);

            $this->eventDispatcher->dispatch(new EditOccurrenceRegistrationEvent($registration));

            $this->addFlash('success', 'registration.edition.success');

            return $this->redirectToRoute('registration_index');
        }

        return $this->render('calendar/registration/edit.html.twig', [
            'occurrence' => $registration->getOccurrence(),
            'registration' => $registration,
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/{id}/annuler', name: 'registration_cancel', methods: ['GET', 'POST'])]
    public function cancel(Registration $registration): Response
    {
        $registration->cancel();
        $this->repository->save($registration);
        $this->eventDispatcher->dispatch(new OccurrenceRegistrationCancelEvent($registration));
        $this->addFlash('success', 'registration.cancel.success');

        return $this->redirectToRoute('registration_index');
    }

    #[Route(path: '/{id}/desanuller', name: 'registration_uncancel', methods: ['GET', 'POST'])]
    public function uncancel(Registration $registration): Response
    {
        if ($registration->getChildrenNumber() + 1 <= $registration->getOccurrence()->getRemainingNeededVolunteers()) {
            $registration->uncancel();
            $this->repository->save($registration);

            $this->addFlash('success', 'registration.uncancel.success');

            return $this->redirectToRoute('registration_index');
        }
        if ($registration->getOccurrence()->getRemainingNeededVolunteers() <= 0) {
            $this->addFlash('danger', 'registration.uncancel.no_vacancies');

            return $this->redirectToRoute('registration_index');
        }
        $this->addFlash('danger', 'registration.uncancel.not_enough_places');

        return $this->redirectToRoute('registration_edit', ['id' => $registration->getId()]);
    }

    #[Route(path: '/{id}', name: 'registration_show', methods: ['GET'])]
    public function show(Registration $registration): Response
    {
        return $this->render('calendar/registration/show.html.twig', [
            'registration' => $registration,
        ]);
    }
}
