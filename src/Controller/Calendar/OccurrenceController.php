<?php

namespace App\Controller\Calendar;

use App\Entity\Occurrence;
use App\Form\Calendar\Occurrence\ContactFormType;
use App\Model\DTO\Calendar\Occurrence\ContactFormDTO;
use App\Repository\OccurrenceRepository;
use App\Repository\RegistrationRepository;
use App\Service\Mailer\ActionMailer;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/calendrier')]
class OccurrenceController extends AbstractController
{
    final public const MAX_OCCURRENCE_PER_PAGE = 12;

    #[Route(path: '', name: 'occurrence_index', methods: ['GET'])]
    public function index(Request $request, OccurrenceRepository $repository, PaginatorInterface $paginator): Response
    {
        $searchTerm = $request->get('search');
        $queryBuilder = $repository->getWithSearchQueryBuilder($searchTerm);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            self::MAX_OCCURRENCE_PER_PAGE
        );

        return $this->render('calendar/occurrence/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route(path: '/occurrence/{id}', name: 'occurrence_show', methods: ['GET', 'POST'])]
    public function show(Request $request, Occurrence $occurrence, ActionMailer $mailer, RegistrationRepository $registrationRepository): Response
    {
        $contactFormDTO = new ContactFormDTO();
        $contactForm = $this->createForm(ContactFormType::class, $contactFormDTO);
        if ($registration = $registrationRepository->findOneBy(['volunteer' => $this->getUser(), 'occurrence' => $occurrence])) {
            $contactForm->handleRequest($request);

            if ($contactForm->isSubmitted() && $contactForm->isValid()) {
                $mailer->sendMessageToReferrer($contactFormDTO, $registration);

                $this->addFlash('success', 'contact-form.success');

                return $this->redirectToRoute('occurrence_show', ['id' => $occurrence->getId()]);
            }
        }

        return $this->render('calendar/occurrence/show.html.twig', [
            'occurrence' => $occurrence,
            'contactForm' => $contactForm->createView(),
        ]);
    }
}
