<?php

namespace App\Model\DTO\Homepage;

use Symfony\Component\Validator\Constraints as Assert;

class NewsletterSubscriptionFormDTO
{
    #[Assert\NotBlank(message: 'contact.email.required')]
    #[Assert\Email(message: 'contact.email.invalid')]
    private ?string $email = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
