<?php

namespace App\Model\DTO\Calendar\Registration;

use App\Entity\Registration;

class RegistrationDTO
{
    private ?int $childrenNumber = null;

    public function __construct(?Registration $registration = null)
    {
        if ($registration) {
            $this->childrenNumber = $registration->getChildrenNumber();
        }
    }

    public function getChildrenNumber(): ?int
    {
        return $this->childrenNumber;
    }

    public function setChildrenNumber(?int $childrenNumber): void
    {
        $this->childrenNumber = $childrenNumber;
    }
}
