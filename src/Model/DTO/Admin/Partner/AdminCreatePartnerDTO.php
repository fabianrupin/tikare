<?php

namespace App\Model\DTO\Admin\Partner;

use App\Validator\Constraints as TiKareAssert;

/**
 * @TiKareAssert\UniqueField(
 *     entityClass="App\Entity\Partner",
 *     fieldName="airTableId",
 *     fieldGetter="getAirTableId",
 *     message="partner.airtable_id.already_exists",
 *     );
 */
class AdminCreatePartnerDTO extends AbstractPartnerDTO
{
}
