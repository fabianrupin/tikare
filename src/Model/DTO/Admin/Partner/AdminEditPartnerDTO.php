<?php

namespace App\Model\DTO\Admin\Partner;

use App\Entity\Partner;

class AdminEditPartnerDTO extends AbstractPartnerDTO
{
    public function __construct(Partner $partner)
    {
        $this->airTableId = $partner->getAirTableId();
        $this->name = $partner->getName();
        $this->description = $partner->getDescription();
        $this->website = $partner->getWebsite();
    }
}
