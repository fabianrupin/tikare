<?php

namespace App\Model\DTO\Admin\Partner;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractPartnerDTO
{
    protected ?string $airTableId = null;

    #[Assert\NotBlank(message: 'partner.name.not_blank')]
    protected ?string $name = null;

    protected ?string $description = null;

    #[Assert\Url(message: 'partner.url.invalid')]
    protected ?string $website = null;

    #[Assert\Image(maxSize: '5M')]
    protected ?File $logo = null;

    public function getAirTableId(): ?string
    {
        return $this->airTableId;
    }

    public function setAirTableId(?string $airTableId): void
    {
        $this->airTableId = $airTableId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getLogo(): ?File
    {
        return $this->logo;
    }

    public function setLogo(?File $logo): void
    {
        $this->logo = $logo;
    }
}
