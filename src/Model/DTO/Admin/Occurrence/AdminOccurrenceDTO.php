<?php

namespace App\Model\DTO\Admin\Occurrence;

use App\Entity\Action;
use App\Entity\Occurrence;
use App\Validator\Constraints as TiKareAssert;
use Carbon\CarbonImmutable;

class AdminOccurrenceDTO
{
    private ?int $totalNeededVolunteers = null;

    private ?int $reservedRegistrations = null;

    private ?int $childrenMinimumAge = null;

    private ?int $childrenMaximumNumber = null;

    /** @TiKareAssert\CarbonDate() */
    private ?CarbonImmutable $startAt = null;

    /** @TiKareAssert\CarbonDate() */
    private ?CarbonImmutable $endsAt = null;

    /** @TiKareAssert\CarbonDate() */
    private ?CarbonImmutable $registerClosedAt = null;

    /** @TiKareAssert\CarbonDate() */
    private ?CarbonImmutable $publishedAt = null;

    private bool $publishable = false;

    private $referrers;

    private ?Action $action = null;

    private ?string $streetNumber = null;

    private ?string $route = null;

    private ?string $locality = null;

    private ?string $postalCode = null;

    public function __construct(?Occurrence $occurrence = null)
    {
        if ($occurrence) {
            $this->totalNeededVolunteers = $occurrence->getTotalNeededVolunteers();
            $this->reservedRegistrations = $occurrence->getReservedRegistrations();
            $this->childrenMinimumAge = $occurrence->getChildrenMinimumAge();
            $this->childrenMaximumNumber = $occurrence->getChildrenMaximumNumber();
            $this->startAt = $occurrence->getStartAt();
            $this->endsAt = $occurrence->getEndsAt();
            $this->registerClosedAt = $occurrence->getRegisterClosedAt();
            $this->publishedAt = $occurrence->getPublishedAt();
            $this->publishable = $occurrence->isPublishable();
            $this->referrers = $occurrence->getReferrers();
            $this->action = $occurrence->getAction();
            if ($address = $occurrence->getAddress()) {
                $this->streetNumber = $address->getStreetNumber();
                $this->route = $address->getRoute();
                $this->locality = $address->getLocality();
                $this->postalCode = $address->getPostalCode();
            }
        }
    }

    public function getTotalNeededVolunteers(): ?int
    {
        return $this->totalNeededVolunteers;
    }

    public function setTotalNeededVolunteers(?int $totalNeededVolunteers): void
    {
        $this->totalNeededVolunteers = $totalNeededVolunteers;
    }

    public function getReservedRegistrations(): ?int
    {
        return $this->reservedRegistrations;
    }

    public function setReservedRegistrations(?int $reservedRegistrations): void
    {
        $this->reservedRegistrations = $reservedRegistrations;
    }

    public function getChildrenMinimumAge(): ?int
    {
        return $this->childrenMinimumAge;
    }

    public function setChildrenMinimumAge(?int $childrenMinimumAge): void
    {
        $this->childrenMinimumAge = $childrenMinimumAge;
    }

    public function getChildrenMaximumNumber(): ?int
    {
        return $this->childrenMaximumNumber;
    }

    public function setChildrenMaximumNumber(?int $childrenMaximumNumber): void
    {
        $this->childrenMaximumNumber = $childrenMaximumNumber;
    }

    public function getStartAt(): ?CarbonImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTime $startAt): void
    {
        if ($startAt) {
            $this->startAt = CarbonImmutable::instance($startAt);
        }
    }

    public function getEndsAt(): ?CarbonImmutable
    {
        return $this->endsAt;
    }

    public function setEndsAt(?\DateTime $endsAt): void
    {
        if ($endsAt) {
            $this->endsAt = CarbonImmutable::instance($endsAt);
        }
    }

    public function getRegisterClosedAt(): ?CarbonImmutable
    {
        return $this->registerClosedAt;
    }

    public function setRegisterClosedAt(?\DateTime $registerClosedAt): void
    {
        if ($registerClosedAt) {
            $this->registerClosedAt = CarbonImmutable::instance($registerClosedAt);
        }
    }

    public function getPublishedAt(): ?CarbonImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTime $publishedAt): void
    {
        if ($publishedAt) {
            $this->publishedAt = CarbonImmutable::instance($publishedAt);
        }
    }

    public function isPublishable(): bool
    {
        return $this->publishable;
    }

    public function setPublishable(bool $publishable): void
    {
        $this->publishable = $publishable;
    }

    public function getReferrers()
    {
        return $this->referrers;
    }

    public function setReferrers($referrers): void
    {
        $this->referrers = $referrers;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): void
    {
        $this->action = $action;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): void
    {
        $this->streetNumber = $streetNumber;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): void
    {
        $this->route = $route;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality): void
    {
        $this->locality = $locality;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }
}
