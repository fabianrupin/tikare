<?php

namespace App\Model\DTO\Admin\Registration;

use App\Entity\Occurrence;
use App\Entity\User;

class AdminRegistrationDTO
{
    private ?User $volunteer = null;

    private ?Occurrence $occurrence = null;

    private int $childrenNumber = 0;

    public function getVolunteer(): ?User
    {
        return $this->volunteer;
    }

    public function setVolunteer(?User $volunteer): void
    {
        $this->volunteer = $volunteer;
    }

    public function getOccurrence(): ?Occurrence
    {
        return $this->occurrence;
    }

    public function setOccurrence(?Occurrence $occurrence): void
    {
        $this->occurrence = $occurrence;
    }

    public function getChildrenNumber(): int
    {
        return $this->childrenNumber;
    }

    public function setChildrenNumber(int $childrenNumber): void
    {
        $this->childrenNumber = $childrenNumber;
    }
}
