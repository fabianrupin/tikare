<?php

namespace App\Model\DTO\Admin\User;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class AdminEditUserDTO extends AbstractUserDTO
{
    #[Assert\NotBlank(message: 'register.email.not_blank')]
    #[Assert\Email(message: 'register.email.invalid')]
    protected ?string $email = null;

    private ?array $roles = [];

    public function __construct(User $user)
    {
        $this->lastName = $user->getLastName();
        $this->firstName = $user->getFirstName();
        $this->phoneNumber = $user->getPhoneNumber();
        $this->birthday = $user->getBirthday();
        $this->district = $user->getDistrict();
        $this->email = $user->getEmail();
        $this->roles = $user->getRoles();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): void
    {
        $this->roles = $roles;
    }
}
