<?php

namespace App\Model\DTO\Admin\User;

use App\Validator\Constraints as TiKareAssert;
use Carbon\CarbonImmutable;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractUserDTO
{
    #[Assert\NotBlank(message: 'register.lastname.not_blank')]
    protected ?string $lastName = null;

    #[Assert\NotBlank(message: 'register.firstname.not_blank')]
    protected ?string $firstName = null;

    /**
     * @TiKareAssert\FrenchMobilePhoneNumber()
     */
    #[Assert\NotBlank(message: 'register.phone_number.not_blank')]
    protected ?string $phoneNumber = null;

    /**
     * @TiKareAssert\CarbonDate()
     */
    #[Assert\NotBlank(message: 'register.birthday.not_blank')]
    #[Assert\LessThanOrEqual('- 16 years', message: 'register.birthday.too_young')]
    protected ?CarbonImmutable $birthday = null;

    #[Assert\NotBlank(message: 'register.district.not_blank')]
    protected ?string $district = null;

    #[Assert\Image(maxSize: '5M')]
    protected ?UploadedFile $profilePicture = null;

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getBirthday(): ?CarbonImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTime $birthday): void
    {
        if ($birthday) {
            $this->birthday = CarbonImmutable::instance($birthday);
        }
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): void
    {
        $this->district = $district;
    }

    public function getProfilePicture(): ?UploadedFile
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?UploadedFile $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }
}
