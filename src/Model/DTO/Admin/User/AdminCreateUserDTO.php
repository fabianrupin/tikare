<?php

namespace App\Model\DTO\Admin\User;

use App\Validator\Constraints as TiKareAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @TiKareAssert\UniqueField(
 *     entityClass="App\Entity\User",
 *     fieldName="email",
 *     fieldGetter="getEmail",
 *     message="register.user.already_exists",
 *     );
 */
class AdminCreateUserDTO extends AbstractUserDTO
{
    #[Assert\NotBlank(message: 'register.email.not_blank')]
    #[Assert\Email(message: 'register.email.invalid')]
    protected ?string $email = null;

    private ?array $roles = [];

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): void
    {
        $this->roles = $roles;
    }
}
