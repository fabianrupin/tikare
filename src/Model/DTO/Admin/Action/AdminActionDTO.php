<?php

namespace App\Model\DTO\Admin\Action;

use App\Entity\Action;
use App\Entity\Partner;
use App\Entity\Thematic;
use App\Validator\Constraints as TiKareAssert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class AdminActionDTO
{
    private ?string $airtableId = null;

    #[Assert\NotBlank(message: 'action.title.not_blank')]
    private ?string $title = null;

    private ?string $description = null;

    #[Assert\Image(maxSize: '5M')]
    protected ?File $picture = null;

    private ?string $contactFirstName = null;

    private ?string $contactLastName = null;

    private ?string $contactRole = null;

    /** @TiKareAssert\FrenchMobilePhoneNumber()*/
    private ?string $contactPhoneNumber = null;

    private ?string $contactEmail = null;

    private ?Partner $partner = null;

    private ?Thematic $thematic = null;

    public function __construct(?Action $action = null)
    {
        if ($action) {
            $this->airtableId = $action->getAirTableId();
            $this->title = $action->getTitle();
            $this->description = $action->getDescription();
            $this->contactFirstName = $action->getContactFirstName();
            $this->contactLastName = $action->getContactLastName();
            $this->contactRole = $action->getContactRole();
            $this->contactPhoneNumber = $action->getContactPhoneNumber();
            $this->contactEmail = $action->getContactEmail();
            $this->partner = $action->getPartner();
            $this->thematic = $action->getThematic();
        }
    }

    public function getAirtableId(): ?string
    {
        return $this->airtableId;
    }

    public function setAirtableId(?string $airtableId): void
    {
        $this->airtableId = $airtableId;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPicture(): ?File
    {
        return $this->picture;
    }

    public function setPicture(?File $picture): void
    {
        $this->picture = $picture;
    }

    public function getContactFirstName(): ?string
    {
        return $this->contactFirstName;
    }

    public function setContactFirstName(?string $contactFirstName): void
    {
        $this->contactFirstName = $contactFirstName;
    }

    public function getContactLastName(): ?string
    {
        return $this->contactLastName;
    }

    public function setContactLastName(?string $contactLastName): void
    {
        $this->contactLastName = $contactLastName;
    }

    public function getContactRole(): ?string
    {
        return $this->contactRole;
    }

    public function setContactRole(?string $contactRole): void
    {
        $this->contactRole = $contactRole;
    }

    public function getContactPhoneNumber(): ?string
    {
        return $this->contactPhoneNumber;
    }

    public function setContactPhoneNumber(?string $contactPhoneNumber): void
    {
        $this->contactPhoneNumber = $contactPhoneNumber;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): void
    {
        $this->partner = $partner;
    }

    public function getThematic(): ?Thematic
    {
        return $this->thematic;
    }

    public function setThematic(?Thematic $thematic): void
    {
        $this->thematic = $thematic;
    }
}
