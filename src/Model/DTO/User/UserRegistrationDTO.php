<?php

namespace App\Model\DTO\User;

use App\Model\DTO\Admin\User\AdminCreateUserDTO;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationDTO extends AdminCreateUserDTO
{
    #[Assert\NotBlank(message: 'register.password.not_blank')]
    #[Assert\Length(min: 6, minMessage: 'register.password.invalid')]
    private ?string $plainPassword = null;

    #[Assert\IsTrue(message: 'register.not_checked')]
    private bool $agreedTerms = false;

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function isAgreedTerms(): bool
    {
        return $this->agreedTerms;
    }

    public function setAgreedTerms(bool $agreedTerms): void
    {
        $this->agreedTerms = $agreedTerms;
    }
}
