<?php

namespace App\Model\DTO\User;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class UploadParentalConsentDTO
{
    #[Assert\File(mimeTypes: ['image/*', 'application/pdf'], mimeTypesMessage: 'parental_consent.invalid_file_type', maxSize: '5M')]
    #[Assert\NotBlank(message: 'parental_consent.not_blank')]
    protected ?UploadedFile $parentalConsentFile = null;

    public function getParentalConsentFile(): ?UploadedFile
    {
        return $this->parentalConsentFile;
    }

    public function setParentalConsentFile(?UploadedFile $parentalConsentFile): void
    {
        $this->parentalConsentFile = $parentalConsentFile;
    }
}
