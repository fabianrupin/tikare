<?php

namespace App\Model\DTO\User;

use App\Entity\User;
use App\Model\DTO\Admin\User\AbstractUserDTO;

class EditUserDTO extends AbstractUserDTO
{
    public function __construct(User $user)
    {
        $this->lastName = $user->getLastName();
        $this->firstName = $user->getFirstName();
        $this->phoneNumber = $user->getPhoneNumber();
        $this->birthday = $user->getBirthday();
        $this->district = $user->getDistrict();
        $this->imageRightClearance = $user->isImageRightClearance();
        $this->newsletter = $user->isNewsletter();
    }

    protected bool $imageRightClearance = true;

    protected bool $newsletter = true;

    public function isImageRightClearance(): bool
    {
        return $this->imageRightClearance;
    }

    public function setImageRightClearance(bool $imageRightClearance): void
    {
        $this->imageRightClearance = $imageRightClearance;
    }

    public function isNewsletter(): bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): void
    {
        $this->newsletter = $newsletter;
    }
}
