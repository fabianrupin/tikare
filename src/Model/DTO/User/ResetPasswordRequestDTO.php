<?php

namespace App\Model\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordRequestDTO
{
    #[Assert\NotBlank(message: 'register.email.not_blank')]
    #[Assert\Email(message: 'register.email.invalid')]
    private ?string $email = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
