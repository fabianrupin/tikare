<?php

namespace App\Security;

use App\Entity\ChangeEmailRequest;
use App\Entity\User;
use App\Repository\ChangeEmailRequestRepository;
use App\Repository\UserRepository;
use App\Service\Mailer\UserMailer;
use Symfony\Component\HttpFoundation\Request;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class EmailVerifier
{
    public function __construct(private readonly VerifyEmailHelperInterface $verifyEmailHelper, private readonly UserRepository $userRepository, private readonly ChangeEmailRequestRepository $changeEmailRequestRepository, private readonly UserMailer $mailer)
    {
    }

    public function sendEmailConfirmation(string $verifyEmailRouteName, ChangeEmailRequest $changeEmailRequest): void
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            $verifyEmailRouteName,
            $changeEmailRequest->getUser()->getId(),
            $changeEmailRequest->getEmail()
        );

        $this->mailer->sendEmailConfirmation($changeEmailRequest, $signatureComponents);
    }

    /**
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, User $user): void
    {
        $changeEmailRequest = $user->getChangeEmailRequest();

        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $changeEmailRequest->getEmail());

        if (!$changeEmailRequest->isExpired()) {
            $user->setEmail($changeEmailRequest->getEmail());
            $this->userRepository->save($user);
            $this->changeEmailRequestRepository->remove($user->getChangeEmailRequest());
        }
    }
}
