<?php

namespace App\Repository;

use App\Entity\Occurrence;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Occurrence|null find($id, $lockMode = null, $lockVersion = null)
 * @method Occurrence|null findOneBy(array $criteria, array $orderBy = null)
 * @method Occurrence[]    findAll()
 * @method Occurrence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OccurrenceRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Occurrence::class);
    }

    public function findTheNext(int $maxResults)
    {
        $qb = $this->getWithSearchQueryBuilder(null);
        $qb->setMaxResults($maxResults);

        return $qb->getQuery()->execute();
    }

    public function getWithSearchQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->findAllQueryBuilder($term);

        return $qb
            ->andWhere('occurrence.publishable = 1')
            ->andWhere('occurrence.publishedAt < CURRENT_TIMESTAMP()')
            ->andWhere('occurrence.endsAt > CURRENT_TIMESTAMP()')
            ->orderBy('occurrence.startAt');
    }

    public function findAllByReferrerQueryBuilder(User $user, ?string $term): QueryBuilder
    {
        $qb = $this->findAllQueryBuilder($term);

        return $qb
            ->innerJoin('occurrence.referrers', 'referrers')
            ->andWhere('referrers = :user')
            ->orderBy('occurrence.startAt')
            ->setParameter('user', $user);
    }

    public function findAllWithoutReferrer(?string $term): QueryBuilder
    {
        $qb = $this->findAllQueryBuilder($term);

        return $qb
            ->innerJoin('occurrence.referrers', 'referrers')
            ->andWhere('referrers IS NULL')
            ->orderBy('occurrence.startAt');
    }

    public function findAllQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('occurrence')
            ->innerJoin('occurrence.action', 'action')
            ->innerJoin('action.partner', 'partner')
            ->innerJoin('action.thematic', 'thematic')
            ->addSelect('occurrence');

        if ($term) {
            $qb->andWhere('action.title LIKE :term OR action.description LIKE :term OR partner.name LIKE :term OR thematic.name LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $qb;
    }
}
