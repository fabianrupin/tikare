<?php

namespace App\Repository;

use App\Entity\Partner;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partner::class);
    }

    public function findAllQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('partner');

        if ($term) {
            $qb->andWhere('partner.name LIKE :term OR partner.description LIKE :term OR partner.website LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $qb;
    }
}
