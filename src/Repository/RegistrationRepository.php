<?php

namespace App\Repository;

use App\Entity\Registration;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Registration|null find($id, $lockMode = null, $lockVersion = null)
 * @method Registration|null findOneBy(array $criteria, array $orderBy = null)
 * @method Registration[]    findAll()
 * @method Registration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistrationRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Registration::class);
    }

    public function findAllQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('registration')
            ->innerJoin('registration.occurrence', 'occurrence')
            ->innerJoin('occurrence.action', 'action')
            ->innerJoin('action.partner', 'partner')
            ->innerJoin('registration.volunteer', 'volunteer')
            ->addSelect('registration');

        if ($term) {
            $qb->andWhere('action.title LIKE :term OR action.description LIKE :term OR partner.name LIKE :term OR volunteer.lastName LIKE :term OR volunteer.firstName LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $qb;
    }

    public function findAllByUserQueryBuilder(UserInterface $user, ?string $term)
    {
        $qb = $this->findAllQueryBuilder($term);
        $qb->andWhere('volunteer = :volunteer')
            ->setParameter('volunteer', $user);

        return $qb;
    }
}
