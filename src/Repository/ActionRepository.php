<?php

namespace App\Repository;

use App\Entity\Action;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Action|null find($id, $lockMode = null, $lockVersion = null)
 * @method Action|null findOneBy(array $criteria, array $orderBy = null)
 * @method Action[]    findAll()
 * @method Action[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Action::class);
    }

    public function findAllQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('action')
            ->leftJoin('action.partner', 'partner');

        if ($term) {
            $qb->andWhere('action.title LIKE :term OR partner.name LIKE :term OR action.contactFirstName LIKE :term OR action.contactLastName LIKE :term OR action.contactPhoneNumber LIKE :term OR action.contactEmail LIKE :term')
                ->setParameter('term', '%'.$term.'%');
        }

        return $qb;
    }
}
