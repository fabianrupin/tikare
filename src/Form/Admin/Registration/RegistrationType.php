<?php

namespace App\Form\Admin\Registration;

use App\Entity\Occurrence;
use App\Entity\User;
use App\Model\DTO\Admin\Registration\AdminRegistrationDTO;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('occurrence', EntityType::class, [
                'label' => 'form.occurrence.label',
                'class' => Occurrence::class,
            ])
            ->add('volunteer', EntityType::class, [
                'label' => 'form.volunteer.label',
                'class' => User::class,
            ])
            ->add('childrenNumber', NumberType::class, [
                'label' => 'form.children_number.label',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdminRegistrationDTO::class,
            'translation_domain' => 'registration',
        ]);
    }
}
