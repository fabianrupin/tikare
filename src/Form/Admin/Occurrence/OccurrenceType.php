<?php

namespace App\Form\Admin\Occurrence;

use App\Entity\Action;
use App\Entity\User;
use App\Model\DTO\Admin\Occurrence\AdminOccurrenceDTO;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OccurrenceType extends AbstractType
{
    private readonly UserRepository $userRepository;

    /*    public function __construct(UserRepository $userRepository){

            $this->userRepository = $userRepository;
        }*/
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*        $referrers = [];
                $users = $this->userRepository->findAll();
                foreach ($users as $user){
                    if($user->isGranted('ROLE_REFERRER')){
                        $referrers[] = $user;
                    }
                }*/

        $builder
            ->add('totalNeededVolunteers', NumberType::class, [
                'label' => 'form.total_needed_volunteers.label',
            ])
            ->add('reservedRegistrations', NumberType::class, [
                'label' => 'form.reserved_registrations.label',
                'required' => false,
            ])
            ->add('childrenMinimumAge', NumberType::class, [
                'label' => 'form.children_minimum_age.label',
                'required' => false,
            ])
            ->add('childrenMaximumNumber', NumberType::class, [
                'label' => 'form.children_maximum_number.label',
                'required' => false,
            ])
            ->add('startAt', DateTimeType::class, [
                'label' => 'form.start_at.label',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('endsAt', DateTimeType::class, [
                'label' => 'form.ends_at.label',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('registerClosedAt', DateTimeType::class, [
                'label' => 'form.register_closed_at.label',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('publishedAt', DateTimeType::class, [
                'label' => 'form.published_at.label',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('publishable', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'label' => 'form.publishable.label',
            ])
            ->add('referrers', EntityType::class, [
                'label' => 'form.referrers.label',
                'class' => User::class,
                'multiple' => true,
                'required' => false,
                'placeholder' => 'nobody',
            ])
            ->add('action', EntityType::class, [
                'label' => 'form.action.label',
                'class' => Action::class,
                'choice_label' => 'title',
            ])
            ->add('streetNumber', TextType::class, [
                'label' => 'form.street_number.label',
                'required' => false,
            ])
            ->add('route', TextType::class, [
                'label' => 'form.route.label',
                'required' => false,
            ])
            ->add('locality', TextType::class, [
                'label' => 'form.locality.label',
                'required' => false,
            ])
            ->add('postalCode', TextType::class, [
                'label' => 'form.postal_code.label',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdminOccurrenceDTO::class,
        ]);
    }
}
