<?php

namespace App\Form\Admin\Action;

use App\Entity\Partner;
use App\Entity\Thematic;
use App\Model\DTO\Admin\Action\AdminActionDTO;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'form.title.label',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'form.description.label',
            ])
            ->add('contactFirstName', TextType::class, [
                'label' => 'form.contact_firstname.label',
                'required' => false,
            ])
            ->add('contactLastName', TextType::class, [
                'label' => 'form.contact_lastname.label',
                'required' => false,
            ])
            ->add('contactRole', TextType::class, [
                'label' => 'form.contact_role.label',
                'required' => false,
            ])
            ->add('contactPhoneNumber', TelType::class, [
                'label' => 'form.contact_phone_number.label',
                'required' => false,
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => 'form.contact_email.label',
                'required' => false,
            ])
            ->add('partner', EntityType::class, [
                'label' => 'form.partner.label',
                'class' => Partner::class,
                'choice_label' => 'name',
            ])
            ->add('thematic', EntityType::class, [
                'label' => 'form.thematic.label',
                'class' => Thematic::class,
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('picture', FileType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdminActionDTO::class,
            'translation_domain' => 'action',
        ]);
    }
}
