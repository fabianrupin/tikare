<?php

namespace App\Form\Admin\Partner;

use App\Model\DTO\Admin\Partner\AbstractPartnerDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('airTableId', TextType::class, [
                'label' => 'form.airtable_id.label',
            ])
            ->add('name', TextType::class, [
                'label' => 'form.name.label',
            ])
            ->add('description', TextType::class, [
                'label' => 'form.description.label',
            ])
            ->add('website', UrlType::class, [
                'label' => 'form.website.label',
            ])
            ->add('logo', FileType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AbstractPartnerDTO::class,
            'translation_domain' => 'partner',
        ]);
    }
}
