<?php

namespace App\Form\Admin\User;

use App\Model\DTO\Admin\User\AdminCreateUserDTO;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUserCreateType extends AbstractUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('email', EmailType::class, [
                'label' => 'admin_create.form.email.label',
            ])
            ->add('profilePicture', FileType::class, [
                'label' => 'admin_create.form.profile_picture.label',
                'required' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'label' => 'admin_create.form.role.label',
                'required' => false,
                'choices' => [
                    'admin_edit.form.role.choice.admin' => 'ROLE_ADMIN',
                    'admin_edit.form.role.choice.referrer' => 'ROLE_REFERRER',
                    'admin_edit.form.role.choice.user' => null,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => AdminCreateUserDTO::class,
            'translation_domain' => 'user',
        ]);
    }
}
