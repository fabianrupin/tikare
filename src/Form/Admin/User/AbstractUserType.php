<?php

namespace App\Form\Admin\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastName', TextType::class, [
                'label' => 'abstract.form.lastname.label',
                'attr' => [
                    'placeholder' => 'abstract.form.lastname.placeholder',
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'abstract.form.firstname.label',
                'attr' => [
                    'placeholder' => 'abstract.form.firstname.placeholder',
                ],
            ])
            ->add('phoneNumber', TelType::class, [
                'label' => 'abstract.form.phone_number.label',
                'attr' => [
                    'placeholder' => 'abstract.form.phone_number.placeholder',
                    'help' => 'abstract.form.phone_number.help',
                ],
            ])
            ->add('birthday', DateType::class, [
                'label' => 'abstract.form.birthday.label',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'html5' => false,
                'attr' => [
                    'class' => 'js-datepicker',
                    'placeholder' => 'abstract.form.birthday.placeholder',
                ],
            ])
            ->add('district', ChoiceType::class, [
                'label' => 'abstract.form.district.label',
                'choices' => [
                    'Rennes' => [
                        'Centre' => 'Rennes - Centre',
                        'Thabor / Saint-Hélier / Alphonse-Guérin' => 'Rennes - Thabor / Saint-Hélier / Alphonse-Guérin',
                        "Bourg L'êveque / La Touche / Moulin du Comte" => "Rennes - Bourg L'êveque / La Touche / Moulin du Comte",
                        'Maurepas / Bellangerais' => 'Rennes - Maurepas / Bellangerais',
                        'Saint-Martin' => 'Rennes - Saint-Martin',
                        "Jeanne d'Arc / Longs Champs / Beaulieu" => "Rennes - Jeanne d'Arc / Longs Champs / Beaulieu",
                        'Francisco Ferrer / Landry / Poterie' => 'Rennes - Francisco Ferrer / Landry / Poterie',
                        'Sud-gare' => 'Rennes - Sud-gare',
                        'Cleunay / Arsenal-Redon / La Courrouze' => 'Rennes - Cleunay / Arsenal-Redon / La Courrouze',
                        'Villejean / Beauregard' => 'Rennes - Villejean / Beauregard',
                        'Le Blosne' => 'Rennes - Le Blosne',
                        'Bréquigny' => 'Rennes - Bréquigny',
                    ],
                    'St-Jacques de la Lande' => 'St-Jacques de la Lande',
                    'Chantepie' => 'Chantepie',
                    'Cesson Sévigné' => 'Cesson Sévigné',
                    'Saint Grégoire' => 'Saint Grégoire',
                    'Betton' => 'Betton',
                    'Bruz' => 'Bruz',
                    'Chavagne' => 'Chavagne',
                    'Montgermont' => 'Montgermont',
                    'Pacé' => 'Pacé',
                    'Le Rheu' => 'Chantepie',
                    'Saint-Sulpice-la-Forêt' => 'Saint-Sulpice-la-Forêt',
                    'Thorigné-Fouillard' => 'Thorigné-Fouillard',
                    'Vern-sur-Seiche' => 'Vern-sur-Seiche',
                    'Vezin-le-Coquet' => 'Vezin-le-Coquet',
                    'Autre' => 'Autre',
                ],
            ])
        ;
    }
}
