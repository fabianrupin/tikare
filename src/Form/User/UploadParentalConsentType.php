<?php

namespace App\Form\User;

use App\Model\DTO\User\UploadParentalConsentDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadParentalConsentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('parentalConsentFile', FileType::class, [
                'label' => 'parental_consent.form.upload_file.label',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => UploadParentalConsentDTO::class,
            'translation_domain' => 'user',
        ]);
    }
}
