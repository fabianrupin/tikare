<?php

namespace App\Form\User;

use App\Model\DTO\User\ChangeEmailDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeEmailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', RepeatedType::class, [
                'type' => EmailType::class,
                'first_options' => [
                    'label' => 'change_email.form.email.label',
                    'attr' => [
                        'placeholder' => 'change_email.form.email.placeholder',
                    ],
                ],
                'second_options' => [
                    'label' => 'change_email.form.repeated_email.label',
                    'attr' => [
                        'placeholder' => 'change_email.form.repeated_email.placeholder',
                    ],
                ],
                'invalid_message' => 'change_email.email_not_equal',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ChangeEmailDTO::class,
            'translation_domain' => 'user',
        ]);
    }
}
