<?php

namespace App\Form\User;

use App\Form\Admin\User\AbstractUserType;
use App\Model\DTO\User\UserRegistrationDTO;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationType extends AbstractUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('email', EmailType::class, [
                'label' => 'register.form.email.label',
                'attr' => [
                    'placeholder' => 'register.form.email.placeholder',
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'first_options' => [
                'label' => 'register.form.password.label',
                'attr' => [
                    'placeholder' => 'register.form.password.placeholder',
                ],
            ],
            'second_options' => [
                'label' => 'register.form.password_confirmation.label',
                'attr' => [
                    'placeholder' => 'register.form.password_confirmation.placeholder',
                ],
            ],
            'invalid_message' => 'register.password_not_equal',
        ])
        ->add('agreedTerms', CheckboxType::class, [
            'label' => 'register.form.agreed_terms.label',
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => UserRegistrationDTO::class,
            'translation_domain' => 'user',
        ]);
    }
}
