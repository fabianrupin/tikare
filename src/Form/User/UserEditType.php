<?php

namespace App\Form\User;

use App\Form\Admin\User\AbstractUserType;
use App\Model\DTO\User\EditUserDTO;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('imageRightClearance', CheckboxType::class, [
                'label' => 'edit.form.image_right_clearance.label',
                'required' => false,
            ])
            ->add('newsletter', CheckboxType::class, [
                'label' => 'edit.form.newsletter.label',
                'required' => false,
            ])
            ->add('profilePicture', FileType::class, [
                'label' => 'edit.form.profile_picture.label',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => EditUserDTO::class,
            'translation_domain' => 'user',
        ]);
    }
}
